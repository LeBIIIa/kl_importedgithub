﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Windows.Forms;
using System.IO;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace KL___WorkingVersion
{
    public partial class Form1 : Form
    {
        const int countTask = 10;
        const int firstPart = 6;
        const int secondPart = 4;
        int variant;
        bool[] tasks;
        List<string> comboList;
        public Form1()
        {
            InitializeComponent();
            comboList = new List<string>(){"1 Частина", "2 Частина", "Усі завдання", "Вибрати окремо" };
            variant1.Checked = true;
            tasks = new bool[countTask];
            for (int i = 0;i<countTask;++i)
            {
                tasks[i] = false;
            }
            TaskComboBox.Items.AddRange(comboList.ToArray());
            TaskComboBox.SelectedIndex = 2;
        }

        private void StartBtn_Click(object sender, EventArgs e)
        {
            ProgressBar.Value = 0;
            Word[] word = new Word[8];
            int count = 0;
            foreach(bool b in tasks)
            {
                if (b)
                {
                    count++;
                }
            }
            ProgressBar.Maximum = count;
            ProgressBar.Step = 1;
            string result = InitializeText();


            if (result.Length != 8)
            {
                MessageBox.Show("Введіть ПІБ 8 символів(не повторюючих)!");
                return;
            }

            InitializeElements(word , result , variant);

            //Створення ПДФ Документу

            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                Filter = "pdf files(*.pdf) | *.pdf",
                FilterIndex = 2,
                RestoreDirectory = true
            };

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = null;
                try
                {
                    Document pdfDocument = new Document();
                    using (fs = new FileStream(saveFileDialog.FileName, FileMode.Create))
                    {
                        string ARIALUNI_TFF = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "ARIALUNI.TTF");
                        string sylfaenpath = Environment.GetEnvironmentVariable("SystemRoot") + "\\fonts\\sylfaen.ttf";
                        BaseFont sylfaen = BaseFont.CreateFont(sylfaenpath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                        Font f = new Font(sylfaen, 12);
                        Font chapterFont = new Font(sylfaen, 16);
                        Font chapterFontPrumitka = new Font(sylfaen, 18);
                        chapterFontPrumitka.SetColor(255, 0, 0);
                        chapterFont.SetColor(13, 0, 129);

                        PdfWriter writer = PdfWriter.GetInstance(pdfDocument, fs);

                        // Виконання завдань
                        pdfDocument.Open();
                        pdfDocument.Add(new Paragraph("Компютерна логіка", chapterFont));
                        pdfDocument.Add(new Paragraph("Увага!!! Текст позначений так '*Примітка'  не потрібно писати.", chapterFontPrumitka));

                        if (tasks[0])
                        {
                            Task1_1 t1_1 = new Task1_1();
                            // Завдання 1.1
                            pdfDocument.Add(new Paragraph("1.1	Скласти шестизначне число, яке складається з отриманих за допомогою кодової таблиці кодів 1-ої, 2-ої та 8-ої літер прізвища. При цьому перші 3 цифри відповідають цілій частині числа, а останні - дробовій.Вважаючи це число десятковим, перевести його до шістнадцяткової, вісімкової та двійкової систем числення з точністю відповідно 3, 3 та 5 розрядів після коми.\n", chapterFont));
                            t1_1.Execute(word, pdfDocument, f);
                            pdfDocument.NewPage();
                            ProgressBar.PerformStep();
                        }
                        if (tasks[1])
                        {
                            Task1_2 t1_2 = new Task1_2();
                            // Завдання 1.2
                            pdfDocument.Add(new Paragraph("1.2	Скласти шестизначне число, яке складається з отриманих за допомогою кодової   таблиці кодів 1-ої, 2-ої та 8-ої літер прізвища. При цьому перші 3 цифри відповідають цілій частині числа, а останні - дробовій.  Вважаючи це число  шістнадцятковим, перевести  його до десяткової, вісімкової та двійкової систем числення з точністю відповідно 3, 3 та 5 розрядів після коми.\n", chapterFont));
                            t1_2.Execute(word, pdfDocument, f);
                            pdfDocument.NewPage();
                            ProgressBar.PerformStep();
                        }
                        // Завдання 1.3
                        if (tasks[2])
                        {
                            Task1_3 t1_3 = new Task1_3();
                            pdfDocument.Add(new Paragraph("1.3	Скласти шестизначне число, яке складається з отриманих за допомогою кодової таблиці кодів 1-ої, 2-ої та 8-ої літер прізвища. Вважаючи це число десятковим, перевести його до системи числення залишкових класів із мінімально необхідною кількістю основ 2, 3, 5, 7, 11, ... . Після цього зробити зворотне переведення отриманого результату до десяткової системи числення.\n", chapterFont));
                            t1_3.Execute(word, pdfDocument, f);
                            pdfDocument.NewPage();
                            ProgressBar.PerformStep();
                        }
                        if (tasks[3])
                        {
                            Task1_4 t1_4 = new Task1_4();
                            // Завдання 1.4
                            pdfDocument.Add(new Paragraph("1.4	Виконати ефективне кодування визначених літер прізвища, при умові, що отримане за допомогою кодової таблиці число - десяткове і говорить про те, скільки разів у повідомленні зустрічається дана літера(при цьому, повідомлення складається всього з 8 обраних літер). Визначити ефективність проведенного кодування та порівняти її з ентропією джерела повідомлення і ефективністю рівномірного кодування, тобто з випадком, коли довжина коду для кожної літери одна й та сама.За допомогою отриманих кодів ∑скласти повідомлення, яке складається з визначених літер у тій послідовності, в якій вони зустрічаються у прізвищі.Визначити довжину(в бітах) повідомлення при ефективному і рівномірному кодуванні.\n\n", chapterFont));
                            t1_4.Execute(word, pdfDocument, f);
                            pdfDocument.NewPage();
                            ProgressBar.PerformStep();
                        }
                        if (tasks[4])
                        {
                            // Завдання 1.5
                            pdfDocument.Add(new Paragraph("1.5 Для шістнадцяти розрядного двійкового коду  (1ц1л)(2ц1л)(1ц8л)(2ц8л) сформувати код Геммінга (Hamming) і продемонструвати його реакцію на однократний збій. Результати подати у вигляді таблиці.\n", chapterFont));
                            Task1_5.Execute(word, pdfDocument, f);
                            pdfDocument.NewPage();
                            ProgressBar.PerformStep();
                        }
                        if (tasks[5])
                        {
                            // Завдвння 1.6
                            pdfDocument.Add(new Paragraph("1.6  Для послідовності 16-кових цифр   (1ц1л)(2ц1л)(1ц2л)(2ц2л)…(1ц8л)(2ц8л), користуючись картами Карно, визначити всі можливі помилкові коди, які можуть виникати при переході від цифри до цифри.\n\n", chapterFont));
                            Task1_6.Execute(word, pdfDocument, f);
                            pdfDocument.NewPage();
                            ProgressBar.PerformStep();
                        }
                        if (tasks[6])
                        {
                            // Завдання 2.1
                            pdfDocument.Add(new Paragraph("2.1    Визначити класи функцій алгебри логіки, до яких належить задана за допомогою таблиці функція трьох змінних(табл.ТZ.2), і її функціональну повноту.Двійкові коди цифр у графі 'f' табл.ТZ.2 потрібно написати вертикально, старший розряд - наверху. \n\n", chapterFont));
                            Task2_1.execute(word, pdfDocument, f);
                            pdfDocument.NewPage();
                            ProgressBar.PerformStep();
                        }

                        if (tasks[7] || tasks[8] || tasks[9])
                        {
                            // Завдання 2.2
                            List<string> ResultForMap2_3 = new List<string>();
                            List<string> ResultForMap2_4 = new List<string>();
                            List<string> funcForCarnoMap = new List<string>();
                            pdfDocument.Add(new Paragraph("2.2 Мінімізувати за допомогою методу Квасна-Мак-Класкі-Петрика 5 функцій (f0, f1,f2, f3, f4) 5 - ти змінних(a, b, c, d, e).Функції задано за допомогою таблиці ТZ.3.Побудуватитаблицю, яка ілюструє процес знаходження простих імплікант, і таблицю покриття (імплікантну таблицю). За допомогою методу Петрика визначити всі мінімальні розв'язки.Кожний третій набір для кожної з функцій має невизначене значення. Відлік починається від першого згори одиничного значення функції з врахуванням зміщення, величина якого позначена у табл.ТZ.3:\n\n", chapterFont));
                            Task2_2.Execute(word, pdfDocument, f, writer, ref funcForCarnoMap, ref ResultForMap2_3, ref ResultForMap2_4);
                            pdfDocument.NewPage();
                            ProgressBar.PerformStep();


                            // Завдання 2.3
                            pdfDocument.Add(new Paragraph("2.3 Мінімізувати за '1' за допомогою карт Карно функції, задані табл.ТZ.3.Після мінімізації доповнити функції сполучними термами, підкреслити вирази для цих термів в аналітичному записі функції і позначити їх на картах Карно.Результат мінімізації повинен співпадати з одним із розв'язків, знайдених за допомогою методу Петрика. \n\n", chapterFont));
                            Task2_3.Execute(word, pdfDocument, f, writer, funcForCarnoMap, ResultForMap2_3);
                            pdfDocument.NewPage();
                            ProgressBar.PerformStep();

                            // Завадання 2.4
                            pdfDocument.Add(new Paragraph("2.4    Мінімізувати за '0' за допомогою карт Карно функції, задані табл. ТZ.3. Після мінімізації доповнити функції сполучними термами, підкреслити вирази для цих термів в аналітичному записі функції і позначити їх на картах Карно. \n\n", chapterFont));
                            Task2_4.Execute(word, pdfDocument, f, writer, funcForCarnoMap, ResultForMap2_4);
                            pdfDocument.NewPage();
                            ProgressBar.PerformStep();
                        }
                        pdfDocument.Close();
                    }
                }
                catch (IOException)
                {
                    MessageBox.Show("Файл використовується іншим процесом!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    if (fs != null)
                    {
                        fs.Close();
                        fs.Dispose();
                    }
                }
            }

        }
        

        public void InitializeElements(Word []word , string x , int variant)
        {
            char sym;
            int v1, v2;
           
            StreamReader temp = new StreamReader(Environment.CurrentDirectory + @"\KLtext.txt");

            string l;
            for (int i = 0; i < 8; i++)
            {
                temp = new StreamReader(Environment.CurrentDirectory + @"\KLtext.txt");
                while (true)
                {
                    l = temp.ReadLine();
                    if (l == x.ElementAt(i).ToString())
                    {
                        sym = l.ElementAt(0);
                        l = temp.ReadLine();
                        v1 = (int)char.GetNumericValue((l.ElementAt(variant*2-2)));
                        l = temp.ReadLine();
                        v2 = (int)char.GetNumericValue((l.ElementAt(variant*2-2)));

                        word[i] = new Word(v1, v2, sym);
                        break;
                    }
                }
            }
            temp.Close();
        }

        public string InitializeText()
        {

            string s = NameTextBox.Text;
            List<char> wor = new List<char>();

            int size = 0;
            int i = 0;
            while (size != 8 && i < s.Length)
            {
                
                char temp = s.ElementAt(i++);
                if ( temp == ' ' )
                {
                    continue;
                }
                int index = wor.FindIndex(x => x == temp);

                if (index == -1)
                {
                    wor.Add(temp);
                    size++;
                }
            }
            return string.Join("", wor);
        }

        private void Variant_CheckedChanged(object sender, EventArgs e)
        {
            switch ((sender as RadioButton).Text)
            {
                case "1":
                    variant = 1;
                    break;
                case "2":
                    variant = 2;
                    break;
                case "3":
                    variant = 3;
                    break;
                case "4":
                    variant = 4;
                    break;
                case "5":
                    variant = 5;
                    break;
                case "6":
                    variant = 6;
                    break;
                case "7":
                    variant = 7;
                    break;
                case "8":
                    variant = 8;
                    break;
                default:
                    MessageBox.Show("Помилка! Не вірний варіант!");
                    break;
            }
        }

        private void TextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && e.KeyChar != Convert.ToChar(Keys.Back))
                e.Handled = true;
            char c = char.ToUpper(e.KeyChar);
            if (e.KeyChar == Convert.ToChar(Keys.Space) && !(sender as TextBox).Text.EndsWith(" "))
                e.Handled = false;
            if (c >= 'A' && c <= 'Z')
                e.Handled = true;
            if (c == 'ё' || c == 'Ё' || c == 'ы' || c == 'Ы' || c == 'Э' || c == 'э' || c == 'Ъ' || c == 'ъ' )
                e.Handled = true;
            
            e.KeyChar = char.ToUpper(e.KeyChar);
        }

        private void TaskComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            int value = (sender as ComboBox).SelectedIndex;
            switch (value)
            {
                case 0:
                    {
                        CheckListTask.Enabled = false;
                        for ( int i = 0;i<countTask;++i )
                        {
                            if ( i < firstPart)
                            {
                                CheckListTask.SetItemCheckState(i, CheckState.Checked);
                                tasks[i] = true;
                            }
                            else
                            {
                                tasks[i] = false;
                                CheckListTask.SetItemCheckState(i, CheckState.Unchecked);
                            }
                        }
                        break;
                    }
                case 1:
                    {
                        CheckListTask.Enabled = false;
                        for (int i = 0; i < countTask; ++i)
                        {
                            if (i >= firstPart && i < (firstPart+secondPart) )
                            {
                                tasks[i] = true;
                                CheckListTask.SetItemCheckState(i, CheckState.Checked);
                            }
                            else
                            {
                                tasks[i] = false;
                                CheckListTask.SetItemCheckState(i, CheckState.Unchecked);
                            }
                        }
                        break;
                    }
                case 2:
                    {
                        CheckListTask.Enabled = false;
                        for (int i = 0; i < countTask; ++i)
                        {
                            CheckListTask.SetItemCheckState(i, CheckState.Checked);
                            tasks[i] = true;
                        }
                        break;
                    }
                case 3:
                    {
                        CheckListTask.Enabled = true;
                        break;
                    }
                default:
                    break;
            }
        }

        private void CheckListTask_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = (sender as CheckedListBox).SelectedIndex;
            tasks[index] ^= true;
        }
        
    }
}
