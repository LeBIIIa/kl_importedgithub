﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KL
{
    class FilterKmapTerms : compareKmapTerms
    {
        private static int temp, temp1, temp2, temp3;

        public static List<char> getTerm(ref List<char> result, int pos)
        {
            int temp;
            List<char> term = new List<char>();
            for (temp = pos; temp < result.Count; temp++)
            {
                if (result[temp] != '+')
                {
                    term.Add(result[temp]);
                }
                else break;
            }
            return term;
        }

        public static int getMintermCount(ref List<char> term)
        {
            int count = 0;
            for (int temp = 0; temp < term.Count; temp++)
            {
                if (char.IsLetter(term[temp]))
                {
                    count++;
                }
            }

            return count;
        }

        public static int getLargestTermSize(ref List<char> result)
        {
            int largestSize = 0;
            List<char> term = new List<char>();

            for (int temp = 0; temp < result.Count; temp += term.Count + 1)
            {
                term = getTerm(ref result, temp);

                if (getMintermCount(ref term) > largestSize)
                {
                    largestSize = getMintermCount(ref term);
                }
            }
            return largestSize;
        }

        public static List<List<char>> getFilterResult(ref List<List<char>> result, ref List<char> essentialTerms)
        {
            List<List<char>> filterResult = new List<List<char>>();
            int filterResultCount = 0;
            int largestCombinationCount = resultTerms;
            List<char> someResult = new List<char>();

            List<List<int>> possibilites = new List<List<int>>();

            for (int temp = 1; temp <= largestCombinationCount; temp++)
            {
                possibilites.Clear();
                possibilites = Combination.getCOmbination(result.Count, temp);

                for (int temp1 = 0; temp1 < possibilites.Count; temp1++)
                {
                    someResult.Clear();

                    for (int temp11 = 0; temp11 < essentialTerms.Count; temp11++)
                        someResult.Add(essentialTerms[temp11]);

                    for (int temp2 = 0; temp2 < possibilites[temp1].Count; temp2++)
                    {
                        if (someResult.Count > 0) someResult.Add('+');
                        for (int temp3 = 0; temp3 < result[possibilites[temp1][temp2]].Count; temp3++)
                        {
                            someResult.Add(result[possibilites[temp1][temp2]][temp3]);
                        }
                    }

                    if (checkResult(someResult, ones))
                    {
                        filterResult.Add(new List<char>());

                        for (int temp4 = 0; temp4 < someResult.Count; temp4++)
                        {
                            filterResult[filterResult.Count - 1].Add(someResult[temp4]);
                        }

                        largestCombinationCount = temp;
                    }
                    if (filterResult.Count == 1)
                    {
                        return filterResult;
                    }
                }
            }

            return filterResult;
        }

        public static bool checkResult(List<char> someResult, List<int> ones)
        {
            List<int> resultPos; //saving someResult positions in k-map

            resultPos = ConverteTerms.termToPos(someResult, ref setKmap.type);

            List<int> tempones = new List<int>();
            for (int i = 0; i < ones.Count; i++)
            {
                tempones.Add(ones[i]);
            }

            for (int temp = 0; temp < resultPos.Count; temp++)
            {
                for (int temp1 = 0; temp1 < tempones.Count; temp1++)
                {
                    if (tempones[temp1] == resultPos[temp])
                    { //searching for matching ones
                        if (tempones.Count == 1)
                        {
                            tempones.Clear();
                        }
                        else
                        {
                            if (temp1 == 0)
                            {
                                tempones.RemoveAt(0);
                            }
                            else
                            {
                                tempones.RemoveRange(0, temp1);
                            }
                        }
                    }//erase matching ones
                }
            }
            if (tempones.Count == 1)
            {
                tempones.Clear();
            }

            //results are covering all ones
            if (tempones.Count == 0)
                return true;
            else//results don't cover all ones
                return false;
        }

        public static List<List<char>> filter(ref List<char> result, List<int> ones)
        {
            List<char> term,    //reading all term alone
        essentialTerms;
            term = new List<char>();
            essentialTerms = new List<char>();//result after filtering
            List<List<char>> filterResult = new List<List<char>>();
            int LargestTermSize;
            List<List<char>> terms = new List<List<char>>();


            //setting essentials
            LargestTermSize = getLargestTermSize(ref result);

            for (int temp = 0; temp < result.Count; temp++)
            {
                term = getTerm(ref result, temp);

                //term is essential 
                if (LargestTermSize > getMintermCount(ref term))
                {
                    //add + for more than one term result
                    if (essentialTerms.Count > 0)
                    {
                        essentialTerms.Add('+');
                    }
                    for (int temp1 = 0; temp1 < term.Count; temp1++)
                    {
                        essentialTerms.Add(term[temp1]); //add term to filter result
                        result.RemoveAt(0 + temp);   //remove term form main result
                    }

                    if (result.Count > 0)//erase + for more than one term
                        result.RemoveAt(0 + temp);

                    resultTerms--;  //decrement result terms
                    temp--;
                }//end if of essential terms
                else
                    temp += term.Count;
            }//end result covering loop

            terms.Add(new List<char>());
            for (int temp = 0; temp < result.Count; temp++)
            {
                if (result[temp] == '+' && temp < result.Count)
                    terms.Add(new List<char>());
                else
                    terms[terms.Count - 1].Add(result[temp]);

            }

            filterResult = getFilterResult(ref terms, ref essentialTerms);

            return filterResult;
        }


    }

    public class Combination
    {
        private static List<int> people = new List<int>();
        private static List<int> element = new List<int>();
        static List<List<int>> result = new List<List<int>>();
        private static int size;
        private static int count;

        public static List<List<int>> getCOmbination(int n, int k)
        {
            size = k;
            people.Clear();
            result.Clear();
            count = 0;
            for (int i = 0; i < n; i++)
            {
                people.Add(i);
            }
            go(0, k);

            return result;
        }

        public static void go(int offset, int k)
        {
            if (k == 0)
            {
                saveResult(ref element, k, ref size);
                return;
            }
            for (int i = offset; i <= people.Count - k; ++i)
            {
                element.Add(people[i]);
                go(i + 1, k - 1);
                element.RemoveAt(element.Count - 1);
            }
        }
        public static void saveResult(ref List<int> v, int k, ref int size)
        {
            result.Add(new List<int>());
            for (int i = 0; i < v.Count; ++i)
            {
                result[count].Add(v[i]);
                if (result[count].Count == size)
                    count++;
            }
        }

    }

}
