﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KL___WorkingVersion
{
    public class Word
    {
        private int value2;
        private char symbol;
        private int value1;
        public Word()
        {
            value1 = 0;
            value2 = 0;
            symbol = '0';
        }
        public Word(int value1, int value2, char symbol)
        {
            this.value1 = value1;
            this.value2 = value2;
            this.symbol = symbol;
        }

        public char Symbol { get => symbol; set => symbol = value; }
        public int Value1 { get => value1; set => value1 = value; }
        public int Value2 { get => value2; set => value2 = value; }
    }
}

