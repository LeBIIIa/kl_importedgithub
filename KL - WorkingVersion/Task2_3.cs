﻿using iTextSharp.text;
using KL___WorkingVersion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text.pdf;
using System.Runtime.InteropServices;
using System.IO;
using KL;



namespace KL___WorkingVersion
{


    static class Task2_3
    {


        public static void Execute(Word[] word, Document pdf, Font f, PdfWriter writer, List<string> fValue, List<string> WordCode)
        {
            List<long> OneValue = new List<long>();
            OneValue = FormOneValue(OneValue, fValue);
            List<string> OneValueHex = FormingHexValue(OneValue);

            List<long> XValue = new List<long>();
            XValue = FormXValue(XValue, fValue);
            List<string> XValueHex = FormingHexValue(XValue);

            FormGreyCode(ref fValue);

            iTextSharp.text.Image img = Image.GetInstance(Environment.CurrentDirectory + @"\CardKarnoFirst.png");
            img.ScaleToFit(500f, 500f);
            img.Alignment = Element.ALIGN_CENTER;
            pdf.Add(img);

            PdfContentByte con = writer.DirectContent;
            con.MoveTo(pdf.PageSize.Width / 2, pdf.PageSize.Height / 2);
            con.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.WINANSI, false), 12);

            int index = 0;
            string FormFValueText = "";


            index = 0; int counter = 0;
            for (int j = 0; j < 4; j++)
            {
                FormFValueText = ""; int tempo = index + 4;
                for (; index < tempo; index++)
                {
                    FormFValueText += fValue.ElementAt(index) + "          ";

                }
                index = tempo;
                con.BeginText();
                con.ShowTextAligned(Element.ALIGN_CENTER, FormFValueText, 180f, (float)(588 - counter), 0f);
                con.EndText();
                con.Stroke();
                counter += 40;
            }

            index = 16; counter = 0;
            for (int j = 0; j < 4; j++)
            {
                FormFValueText = ""; int tempo = index + 4;
                for (; index < tempo; index++)
                {
                    FormFValueText += fValue.ElementAt(index) + "          ";

                }
                index = tempo;
                con.BeginText();
                con.ShowTextAligned(Element.ALIGN_CENTER, FormFValueText, 425f, (float)(588 - counter), 0f);
                con.EndText();
                con.Stroke();
                counter += 40;

            }


             /*  con.SetColorStroke(new BaseColor(255 , 0 , 0 , 150));
               con.SetLineWidth(5);
               con.MoveTo(330, 610);
               con.LineTo(335, 450);
               con.Stroke();  */


            // малювання квадратів
            // =====================
            // =====================
            List<Conections> conCells = new List<Conections>();
            //--------------------------------------------------------
            //======================================================== перша карта
            //--------------------------------------------------------
            conCells.Add(new Conections("0", "8", "1", "4", "2")); conCells[0].setCoordinats(95, 610);
            conCells.Add(new Conections("1", "9", "3", "5", "0")); conCells[1].setCoordinats(127, 610);
            conCells.Add(new Conections("3", "B", "2", "7", "1")); conCells[2].setCoordinats(167, 610);
            conCells.Add(new Conections("2", "A", "0", "6", "3")); conCells[3].setCoordinats(207, 610);
            //--------------------------------------------------------
            conCells.Add(new Conections("4", "0", "5", "C", "6")); conCells[4].setCoordinats(95, 570);
            conCells.Add(new Conections("5", "1", "7", "D", "4")); conCells[5].setCoordinats(127, 570);
            conCells.Add(new Conections("7", "3", "6", "F", "5")); conCells[6].setCoordinats(167, 570);
            conCells.Add(new Conections("6", "2", "4", "E", "7")); conCells[7].setCoordinats(207, 570);
            //--------------------------------------------------------
            conCells.Add(new Conections("C", "4", "D", "8", "E")); conCells[8].setCoordinats(95, 530);
            conCells.Add(new Conections("D", "5", "F", "9", "C")); conCells[9].setCoordinats(127, 530);
            conCells.Add(new Conections("F", "7", "E", "B", "D")); conCells[10].setCoordinats(167, 530);
            conCells.Add(new Conections("E", "6", "C", "A", "F")); conCells[11].setCoordinats(207, 530);
            //--------------------------------------------------------
            conCells.Add(new Conections("8", "C", "9", "0", "A")); conCells[12].setCoordinats(95, 490);
            conCells.Add(new Conections("9", "D", "B", "1", "8")); conCells[13].setCoordinats(127, 490);
            conCells.Add(new Conections("B", "F", "A", "3", "9")); conCells[14].setCoordinats(167, 490);
            conCells.Add(new Conections("A", "E", "8", "2", "B")); conCells[15].setCoordinats(207, 490);
            //--------------------------------------------------------
            //======================================================== друга карта
            //--------------------------------------------------------
            conCells.Add(new Conections("10", "18", "11", "14", "12")); conCells[16].setCoordinats(330, 610);
            conCells.Add(new Conections("11", "19", "13", "15", "10")); conCells[17].setCoordinats(363, 610);
            conCells.Add(new Conections("13", "1B", "12", "17", "11")); conCells[18].setCoordinats(403, 610);
            conCells.Add(new Conections("12", "1A", "10", "16", "13")); conCells[19].setCoordinats(443, 610);
            //--------------------------------------------------------/
            conCells.Add(new Conections("14", "10", "15", "1C", "16")); conCells[20].setCoordinats(330, 570);
            conCells.Add(new Conections("15", "11", "17", "1D", "14")); conCells[21].setCoordinats(363, 570);
            conCells.Add(new Conections("17", "13", "16", "1F", "15")); conCells[22].setCoordinats(403, 570);
            conCells.Add(new Conections("16", "12", "14", "1E", "17")); conCells[23].setCoordinats(443, 570);
            //--------------------------------------------------------
            conCells.Add(new Conections("1C", "14", "1D", "18", "1E")); conCells[24].setCoordinats(330, 530);
            conCells.Add(new Conections("1D", "15", "1F", "19", "1C")); conCells[25].setCoordinats(363, 530);
            conCells.Add(new Conections("1F", "17", "1E", "1B", "1D")); conCells[26].setCoordinats(403, 530);
            conCells.Add(new Conections("1E", "16", "1C", "1A", "1F")); conCells[27].setCoordinats(443, 530);
            //--------------------------------------------------------
            conCells.Add(new Conections("18", "1C", "19", "10", "1A")); conCells[28].setCoordinats(330, 490);
            conCells.Add(new Conections("19", "1D", "1B", "11", "18")); conCells[29].setCoordinats(363, 490);
            conCells.Add(new Conections("1B", "1F", "1A", "13", "19")); conCells[30].setCoordinats(403, 490);
            conCells.Add(new Conections("1A", "1E", "18", "12", "1B")); conCells[31].setCoordinats(443, 490);
            //--------------------------------------------------------
            //========================================================
            //--------------------------------------------------------
            List<BaseColor> colorRec = new List<BaseColor>();
            colorRec.Add(new BaseColor(255, 0, 0 , 200));
            colorRec.Add(new BaseColor(0, 255, 0 , 200));
            colorRec.Add(new BaseColor(0, 0, 255 , 200));
            colorRec.Add(new BaseColor(255, 247, 0 , 200));
            colorRec.Add(new BaseColor(255, 0, 230 , 200));
            colorRec.Add(new BaseColor(0, 255, 255 , 200));
            colorRec.Add(new BaseColor(255, 154, 0 , 200));
            colorRec.Add(new BaseColor(171, 0, 255 , 200));
            colorRec.Add(new BaseColor(0, 255, 154 , 200));

            pdf.Add(new Paragraph(new Phrase("Мінімізація функції f0 \n", f)));

            // Мінімізація функції


            List<string> tempTerm = new List<string>();
            for (int i = 0; i < WordCode.Count; i++)
            {
                string term = "";
                for (int j = 0; j < WordCode.ElementAt(i).Length; j++)
                {
                    if (WordCode[i][j] == '_')
                    {
                        term += "-";
                    }
                    else term += WordCode[i][j];
                }
                tempTerm.Add(term);
            }
            WordCode = tempTerm;
            WordCode = WordCode.Distinct().ToList();

            string temp = "";

            // видалення існуючих сполучних термів так як вони не впливають на функцію
            List<string> BinaryTerms;
            BinaryTerms = RemoveConcatenceTerm(ref WordCode);


            List<string> MinimizedXValue = new List<string>(); int RecSizeCounter = 0; con.SetLineWidth(7);
            // Виведення значень на екран
            for (int i = 0; i < WordCode.Count; i++)
            {
                
                temp = "";
                temp += (i + 1).ToString() + ") Клітинки ";
                List<string> cells = new List<string>();
                ShowMinimizeCell(ref cells, WordCode.ElementAt(i));
                for (int j = 0; j < cells.Count - 1; j++)
                {
                    temp += cells[j] + ", ";
                }
                temp += cells[cells.Count - 1];
                temp += " | Результат = " + WordCode.ElementAt(i) + ". Мінімізуються набори : ";
                for (int j = 0; j < cells.Count; j++)
                {
                    if (OneValueHex.Contains(cells[j]))
                    {
                        temp += cells[j] + " ";
                    }
                    else MinimizedXValue.Add(cells[j]);
                }
                pdf.Add(new Paragraph(temp, f));

                // малювання квадратів
                if (i < 9)
                {
                    con.SetColorStroke(colorRec[i]); 
                }
                con.SetLineWidth(7 - RecSizeCounter);
                for (int k = 0; k < cells.Count; k++)
                {
                    List<int> waysToDraw = new List<int>();
                    string tempRec = cells[k];
                    string up = "", right = "", down = "", left = "";
                    int x = 0, y = 0;
                    for (int j = 0; j < conCells.Count; j++)
                    {
                        if (conCells[j].Center == tempRec)
                        {
                            up = conCells[j].up;
                            right = conCells[j].right;
                            down = conCells[j].down;
                            left = conCells[j].left;
                            x = conCells[j].x;
                            y = conCells[j].y;
                            break;
                        }
                    }
                    if (!cells.Contains(up)) { waysToDraw.Add(0); }
                    if (!cells.Contains(right)) { waysToDraw.Add(1); }
                    if (!cells.Contains(down)) { waysToDraw.Add(2); }
                    if (!cells.Contains(left)) { waysToDraw.Add(3); }

                    if (waysToDraw.Count == 0) continue; // Якщо елементів немає то нічого не малюєм
                    else if (waysToDraw.Count == 1) // якщо немає сусіда з одного боку
                    {
                        switch (waysToDraw[0])
                        {
                            case 0: // зверху
                                {
                                    con.MoveTo(x, y - 4);
                                    con.LineTo(x + 38, y - 4);
                                    con.Stroke();
                                    break;
                                }
                            case 1: // справа
                                {
                                    con.MoveTo(x + 36, y);
                                    con.LineTo(x + 36, y - 40);
                                    con.Stroke();
                                    break;
                                }
                            case 2: // знизу
                                {
                                    con.MoveTo(x, y - 36);
                                    con.LineTo(x + 40, y - 36);
                                    con.Stroke();
                                    break;
                                }
                            case 3: // зліва
                                {
                                    con.MoveTo(x + 4, y);
                                    con.LineTo(x + 4, y - 40);
                                    con.Stroke();
                                    break;
                                }
                        }
                    }
                    else if (waysToDraw.Count == 2)
                    {
                        if (Math.Abs(waysToDraw[0] - waysToDraw[1]) == 2)
                        {
                            if (waysToDraw[0] == 1 || waysToDraw[0] == 3)
                            {
                                con.MoveTo(x + 4, y);
                                con.LineTo(x + 4, y - 40);
                                con.Stroke();

                                con.MoveTo(x + 36, y);
                                con.LineTo(x + 36, y - 40);
                                con.Stroke();
                            }
                            else
                            {
                                con.MoveTo(x, y - 4);
                                con.LineTo(x + 40, y - 4);
                                con.Stroke();

                                con.MoveTo(x, y - 36);
                                con.LineTo(x + 40, y - 36);
                                con.Stroke();
                            }
                        }
                        else
                        {
                            if (waysToDraw[0] == 0 && waysToDraw[1] == 1)
                            {
                                con.MoveTo(x, y - 4);
                                con.LineTo(x + 36, y - 4);
                                con.LineTo(x + 36, y - 40);
                                con.Stroke();
                            }
                            else if (waysToDraw[0] == 0 && waysToDraw[1] == 3)
                            {
                                con.MoveTo(x + 4, y - 40);
                                con.LineTo(x + 4, y - 4);
                                con.LineTo(x + 40, y - 4);
                                con.Stroke();
                            }
                            else if (waysToDraw[0] == 1 && waysToDraw[1] == 2)
                            {
                                con.MoveTo(x + 36, y);
                                con.LineTo(x + 36, y - 36);
                                con.LineTo(x, y - 36);
                                con.Stroke();
                            }
                            else if (waysToDraw[0] == 2 && waysToDraw[1] == 3)
                            {
                                con.MoveTo(x + 4, y);
                                con.LineTo(x + 4, y - 36);
                                con.LineTo(x + 40, y - 36);
                                con.Stroke();
                            }
                        }
                    }
                    else if (waysToDraw.Count == 3)
                    {
                        if (waysToDraw[0] == 0 && waysToDraw[1] == 1 && waysToDraw[2] == 2)
                        {
                            con.MoveTo(x, y - 4);
                            con.LineTo(x + 36, y - 4);
                            con.LineTo(x + 36, y - 36);
                            con.LineTo(x, y - 36);
                            con.Stroke();
                        }
                        else if (waysToDraw[0] == 1 && waysToDraw[1] == 2 && waysToDraw[2] == 3)
                        {
                            con.MoveTo(x + 4, y);
                            con.LineTo(x + 4, y - 36);
                            con.LineTo(x + 36, y - 36);
                            con.LineTo(x + 36, y);
                            con.Stroke();
                        }
                        else if (waysToDraw[0] == 0 && waysToDraw[1] == 2 && waysToDraw[2] == 3)
                        {
                            con.MoveTo(x + 40, y - 4);
                            con.LineTo(x + 4, y - 4);
                            con.LineTo(x + 4, y - 36);
                            con.LineTo(x + 40, y - 36);
                            con.Stroke();
                        }
                        else if (waysToDraw[0] == 0 && waysToDraw[1] == 1 && waysToDraw[2] == 3)
                        {
                            con.MoveTo(x + 4, y - 40);
                            con.LineTo(x + 4, y - 4);
                            con.LineTo(x + 36, y - 4);
                            con.LineTo(x + 36, y - 40);
                            con.Stroke();
                        }
                    }
                    else if (waysToDraw.Count == 4)
                    {
                        con.MoveTo(x + 4, y - 4);
                        con.LineTo(x + 36, y - 4);
                        con.LineTo(x + 36, y - 36);
                        con.LineTo(x + 4, y - 36);
                        con.LineTo(x + 4, y - 4);
                        con.Stroke();
                    }

                }
                ++RecSizeCounter;
                ++RecSizeCounter;
                if (RecSizeCounter > 5) RecSizeCounter = 3;

            }
            MinimizedXValue.Distinct();
            temp = "Клітинки : (";
            for (int i = 0; i < MinimizedXValue.Count - 1; i++)
            {
                temp += MinimizedXValue[i] + ", ";
            }
            temp += MinimizedXValue[MinimizedXValue.Count - 1] + " )";
            temp += " довизначаємо до '1' оскільки вони беруть участь у спрощенні.";
            pdf.Add(new Paragraph(temp, f));

            pdf.Add(new Paragraph("\nВизначення сполучних термів:", f));
            PdfPTable termImlicantTable = new PdfPTable(2);
            termImlicantTable.WidthPercentage = 25;
            termImlicantTable.AddCell(new PdfPCell(new Phrase("Номер імліканти", f)));
            termImlicantTable.AddCell(new PdfPCell(new Phrase("abcde", f)));
            for (int i = 0; i < BinaryTerms.Count; i++)
            {
                termImlicantTable.AddCell(new PdfPCell(new Phrase("i" + i.ToString(), f)));
                termImlicantTable.AddCell(new PdfPCell(new Phrase(BinaryTerms.ElementAt(i), f)));
            }
            pdf.Add(termImlicantTable); pdf.Add(new Paragraph("\n", f));

            PdfPTable ConcatenceTerm = new PdfPTable(3);
            List<string> SaveConcatenceTerm = new List<string>();
            ConcatenceTerm.WidthPercentage = 43;

            ConcatenceTerm.AddCell(new PdfPCell(new Paragraph("Порівнюються", f)));
            ConcatenceTerm.AddCell(new PdfPCell(new Paragraph("Результат", f)));
            ConcatenceTerm.AddCell(new PdfPCell(new Paragraph("Терм", f)));

            for (int i = 0; i < BinaryTerms.Count; i++)
            {
                for (int j = i + 1; j < BinaryTerms.Count; j++)
                {
                    ConcatenceTerm.AddCell(new PdfPCell(new Paragraph("i" + i.ToString() + " i" + j.ToString(), f)));

                    int XCount = 0;
                    string resultTerm = ResultConcatenceTerm(BinaryTerms.ElementAt(i), BinaryTerms.ElementAt(j), ref XCount);
                    ConcatenceTerm.AddCell(new PdfPCell(new Paragraph(resultTerm, f)));
                    if (XCount == 1)
                    {
                        RemoveX(ref resultTerm);
                        ConcatenceTerm.AddCell(new PdfPCell(new Paragraph(resultTerm, f)));
                        SaveConcatenceTerm.Add(resultTerm);
                    }
                    else
                    {

                        ConcatenceTerm.AddCell(new PdfPCell(new Paragraph("Немає", f)));
                    }
                }
            }
            pdf.Add(ConcatenceTerm);

            temp = "Функція з сполучними термами \nf = ";
            TranslateToBinary(ref SaveConcatenceTerm);
            WordCode.AddRange(SaveConcatenceTerm);
            WordCode = WordCode.Distinct().ToList();

            for (int i = 0; i < WordCode.Count - 1; i++)
            {
                temp += WordCode[i] + " v ";
            }
            temp += WordCode[WordCode.Count - 1];
            pdf.Add(new Paragraph(temp, f));

           
        }


        public class Conections
        {
          public  string Center;

           public int x, y;
          public  string up { get; set; }
           public string right { get; set; }
           public string down { get; set; }
           public string left { get; set; }
          public  Conections( string c ,  string u , string r , string d , string l)
            {
                Center = c;
                up = u;
                right = r;
                down = d;
                left = l;
            }
            public void setCoordinats(int x , int y)
            {
                this.x = x;
                this.y = y;
            }
        }

        public static void TranslateToBinary(ref List<string> val)
        {
            List<string> result = new List<string>();
            for (int i = 0; i < val.Count; i++)
            {
                string temp = ""; int count = 0;
                for (int j = 0; j < val[i].Length; j++)
                {
                    if (val[i][j] == '-') { temp += "-"; count++; }
                    else
                    {
                        if (val[i][j] == '0')
                        {
                            switch (count)
                            {
                                case 0: { temp += "/a"; count++; break; }
                                case 1: { temp += "/b"; count++; break; }
                                case 2: { temp += "/c"; count++; break; }
                                case 3: { temp += "/d"; count++; break; }
                                case 4: { temp += "/e"; count++; break; }
                            }
                        }
                        else
                        {
                            switch (count)
                            {
                                case 0: { temp += "a"; count++; break; }
                                case 1: { temp += "b"; count++; break; }
                                case 2: { temp += "c"; count++; break; }
                                case 3: { temp += "d"; count++; break; }
                                case 4: { temp += "e"; count++; break; }
                            }
                        }
                    }
                }
                result.Add(temp);
            }
            val = result;
        }

        public static List<string> FormingHexValue(List<long> Value)
        {
            List<string> result = new List<string>();
            for (int i = 0; i < Value.Count; i++)
            {
                switch (Value[i])
                {
                    case 0: { result.Add("0"); break; }
                    case 1: { result.Add("1"); break; }
                    case 2: { result.Add("2"); break; }
                    case 3: { result.Add("3"); break; }

                    case 4: { result.Add("4"); break; }
                    case 5: { result.Add("5"); break; }
                    case 6: { result.Add("6"); break; }
                    case 7: { result.Add("7"); break; }

                    case 8: { result.Add("8"); break; }
                    case 9: { result.Add("9"); break; }
                    case 10: { result.Add("A"); break; }
                    case 11: { result.Add("B"); break; }

                    case 12: { result.Add("C"); break; }
                    case 13: { result.Add("D"); break; }
                    case 14: { result.Add("E"); break; }
                    case 15: { result.Add("F"); break; }

                    case 16: { result.Add("10"); break; }
                    case 17: { result.Add("11"); break; }
                    case 18: { result.Add("12"); break; }
                    case 19: { result.Add("13"); break; }

                    case 20: { result.Add("14"); break; }
                    case 21: { result.Add("15"); break; }
                    case 22: { result.Add("16"); break; }
                    case 23: { result.Add("17"); break; }

                    case 24: { result.Add("18"); break; }
                    case 25: { result.Add("19"); break; }
                    case 26: { result.Add("1A"); break; }
                    case 27: { result.Add("1B"); break; }

                    case 28: { result.Add("1C"); break; }
                    case 29: { result.Add("1D"); break; }
                    case 30: { result.Add("1E"); break; }
                    case 31: { result.Add("1F"); break; }

                }
            }
            return result;
        }

        public static void RemoveX(ref string str)
        {
            string result = "";
            for (int i = 0; i < str.Length; i++)
            {
                if (str.ElementAt(i) == 'X') result += "-";
                else result += str[i];
            }
            str = result;
        }

        public static void FormGreyCode(ref List<string> fvalue)
        {
            List<string> result = new List<string>();
            result.Add(fvalue[0]);
            result.Add(fvalue[1]);
            result.Add(fvalue[3]);
            result.Add(fvalue[2]);

            result.Add(fvalue[4]);
            result.Add(fvalue[5]);
            result.Add(fvalue[7]);
            result.Add(fvalue[6]);

            result.Add(fvalue[12]);
            result.Add(fvalue[13]);
            result.Add(fvalue[15]);
            result.Add(fvalue[14]);

            result.Add(fvalue[8]);
            result.Add(fvalue[9]);
            result.Add(fvalue[11]);
            result.Add(fvalue[10]);


            result.Add(fvalue[16]);
            result.Add(fvalue[17]);
            result.Add(fvalue[19]);
            result.Add(fvalue[18]);

            result.Add(fvalue[20]);
            result.Add(fvalue[21]);
            result.Add(fvalue[23]);
            result.Add(fvalue[22]);

            result.Add(fvalue[28]);
            result.Add(fvalue[29]);
            result.Add(fvalue[31]);
            result.Add(fvalue[30]);

            result.Add(fvalue[24]);
            result.Add(fvalue[25]);
            result.Add(fvalue[27]);
            result.Add(fvalue[26]);

            fvalue = result;
        }

        public static string ResultConcatenceTerm(string s1, string s2, ref int xCount)
        {
            bool result = false;
            string temp1, temp2;
            string term = "";

            for (int i = 0; i < s1.Count(); i++)
            {

                temp1 = s1.ElementAt(i).ToString(); temp2 = s2.ElementAt(i).ToString();
                if ((temp1 == "-" && temp2 == "0") || (temp2 == "-" && temp1 == "0") || (temp1 == "0" && temp2 == "0"))
                {
                    term += "0";
                }
                else if ((temp1 == "-" && temp2 == "1") || (temp2 == "-" && temp1 == "1") || (temp1 == "1" && temp2 == "1"))
                {
                    term += "1";
                }
                else if ((temp1 == "0" && temp2 == "1") || (temp1 == "1" && temp2 == "0"))
                {
                    xCount++;
                    term += "X";
                }
                else { term += "-"; }
            }

            return term;


        }

        public static void ShowMinimizeCell(ref List<string> temp, string term)
        {
            List<string> A = new List<string>() { "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "1A", "1B", "1C", "1D", "1E", "1F" };
            List<string> NotA = new List<string>() { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F" };

            List<string> B = new List<string>() { "C", "D", "F", "E", "8", "9", "B", "A", "1C", "1D", "1F", "1E", "18", "19", "1B", "1A" };
            List<string> NotB = new List<string>() { "0", "1", "3", "2", "4", "5", "6", "7", "10", "11", "13", "12", "14", "15", "16", "17" };

            List<string> C = new List<string>() { "4", "5", "6", "7", "C", "D", "E", "F", "14", "15", "16", "17", "1C", "1D", "1E", "1F" };
            List<string> NotC = new List<string>() { "0", "1", "3", "2", "8", "9", "B", "A", "10", "11", "13", "12", "18", "19", "1B", "1A" };

            List<string> D = new List<string>() { "3", "2", "7", "6", "F", "E", "B", "A", "13", "12", "17", "16", "1F", "1E", "1B", "1A" };
            List<string> NotD = new List<string>() { "0", "1", "4", "5", "С", "D", "8", "9", "10", "11", "14", "15", "1C", "1D", "18", "19" };

            List<string> E = new List<string>() { "1", "3", "5", "7", "D", "F", "9", "B", "11", "13", "15", "17", "1D", "1F", "19", "1B" };
            List<string> NotE = new List<string>() { "0", "4", "C", "8", "2", "6", "E", "A", "10", "14", "1C", "18", "12", "16", "1E", "1A" };

            string newTerm = "";
            for (int i = 0; i < term.Count(); i++)
            {
                if (term.ElementAt(i) != '-')
                    newTerm += term.ElementAt(i);
            }
            List<string> intersectTerm = new List<string>()
            { "0" , "1" , "3" , "2" ,
              "4" , "5" , "7" , "6" ,
              "C" , "D" , "F" , "E" ,
              "8" , "9" , "B" , "A" ,

              "10" , "11" , "13" , "12" ,
              "14" , "15" , "17" , "16" ,
              "1C" , "1D" , "1F" , "1E" ,
              "18" , "19" , "1B" , "1A" ,
            };


            for (int j = 0; j < newTerm.Count(); j++)
            {
                switch (newTerm.ElementAt(j).ToString())
                {
                    case "/":
                        {
                            j++;
                            switch (newTerm.ElementAt(j).ToString())
                            {
                                case "a":
                                    {
                                        intersectNewTerm(ref intersectTerm, NotA);
                                        //  intersectTerm = intersectTerm.Intersect(NotA).ToList();
                                        break;
                                    }
                                case "b":
                                    {
                                        intersectNewTerm(ref intersectTerm, NotB);
                                        //  intersectTerm = intersectTerm.Intersect(NotB).ToList();
                                        break;
                                    }
                                case "c":
                                    {
                                        intersectNewTerm(ref intersectTerm, NotC);
                                        //  intersectTerm = intersectTerm.Intersect(NotC).ToList();
                                        break;
                                    }
                                case "d":
                                    {
                                        intersectNewTerm(ref intersectTerm, NotD);
                                        //  intersectTerm = intersectTerm.Intersect(NotD).ToList();

                                        break;
                                    }
                                case "e":
                                    {
                                        intersectNewTerm(ref intersectTerm, NotE);
                                        // intersectTerm = intersectTerm.Intersect(NotE).ToList();

                                        break;
                                    }
                            }
                            break;
                        }
                    default:
                        {
                            switch (newTerm.ElementAt(j).ToString())
                            {
                                case "a":
                                    {
                                        intersectNewTerm(ref intersectTerm, A);
                                        //  intersectTerm = intersectTerm.Intersect(A).ToList();
                                        break;
                                    }
                                case "b":
                                    {
                                        intersectNewTerm(ref intersectTerm, B);
                                        break;
                                    }
                                case "c":
                                    {
                                        intersectNewTerm(ref intersectTerm, C);
                                        //  intersectTerm = intersectTerm.Intersect(C).ToList();
                                        break;
                                    }
                                case "d":
                                    {
                                        intersectNewTerm(ref intersectTerm, D);
                                        // intersectTerm = intersectTerm.Intersect(D).ToList();

                                        break;
                                    }
                                case "e":
                                    {
                                        intersectNewTerm(ref intersectTerm, E);
                                        //  intersectTerm = intersectTerm.Intersect(E).ToList();

                                        break;
                                    }
                            }

                            break;
                        }
                }
            }

            for (int k = 0; k < intersectTerm.Count; k++)
            {
                temp.Add(intersectTerm[k]);
            }
        }

        public static void intersectNewTerm(ref List<string> Intersect, List<string> compare)
        {
            List<string> resultIntersect = new List<string>();
            for (int i = 0; i < Intersect.Count; i++)
            {
                for (int j = 0; j < compare.Count; j++)
                {
                    if (Intersect.ElementAt(i) == compare.ElementAt(j))
                    {
                        resultIntersect.Add(Intersect.ElementAt(i));
                        break;
                    }
                }
            }
            Intersect = resultIntersect;
        }

        public static List<string> RemoveConcatenceTerm(ref List<string> wordsByte)
        {

            string result = "";
            List<string> temp = new List<string>();
            for (int i = 0; i < wordsByte.Count; i++)
            {
                result = "";
                for (int j = 0; j < wordsByte[i].Length; j++)
                {
                    switch (wordsByte[i][j])
                    {
                        case '-': { result += "-"; break; }
                        case '/': { result += "0"; j++; break; }
                        default: { result += "1"; break; }
                    }
                }
                temp.Add(result);
            }


            for (int i = 0; i < wordsByte.Count; i++)
            {
                for (int j = i + 1; j < wordsByte.Count; j++)
                {
                    string resultTerm;
                    bool res = FindConcatenceTerm(temp.ElementAt(i), temp.ElementAt(j), out resultTerm);
                    if (res)
                    {
                        RemoveTerm(ref wordsByte, resultTerm, temp, ref temp);

                    }
                }

            }

            return temp;
        }

        public static void RemoveTerm(ref List<string> Terms, string remove, List<string> compare, ref List<string> temp)
        {
            List<string> resultBinaryTerms = new List<string>();
            List<string> returnTermList = new List<string>();
            for (int i = 0; i < Terms.Count; i++)
            {
                if (compare.ElementAt(i) == remove) { continue; }
                else
                {
                    resultBinaryTerms.Add(temp.ElementAt(i));
                    returnTermList.Add(Terms.ElementAt(i));
                }
            }
            Terms = returnTermList;
            temp = resultBinaryTerms;
        }

        public static bool FindConcatenceTerm(string s1, string s2, out string returnTerm)
        {
            bool result = false;
            string temp1, temp2;
            string term = "";
            int xCount = 0;
            for (int i = 0; i < s1.Count(); i++)
            {

                temp1 = s1.ElementAt(i).ToString(); temp2 = s2.ElementAt(i).ToString();
                if ((temp1 == "-" && temp2 == "0") || (temp2 == "-" && temp1 == "0") || (temp1 == "0" && temp2 == "0"))
                {
                    term += "0";
                }
                else if ((temp1 == "-" && temp2 == "1") || (temp2 == "-" && temp1 == "1") || (temp1 == "1" && temp2 == "1"))
                {
                    term += "1";
                }
                else if ((temp1 == "0" && temp2 == "1") || (temp1 == "1" && temp2 == "0"))
                {
                    xCount++;
                    term += "-";
                }
                else { term += "-"; }
            }
            returnTerm = term;
            if (xCount == 1) return true;
            else
            {
                return result;
            }
        }

        public static bool Find(string str, string s)
        {
            bool result = false;
            string comparer;
            for (int i = 0; i < s.Length; i++)
            {
                char compare = s[i];
                for (int j = 0; j < str.Length; j++)
                {
                    if (str[j] == compare)
                    {

                    }
                }
            }

            return result;
        }

        public static List<long> FormOneValue(List<long> OneValue, List<string> fValue)
        {
            for (int i = 0; i < fValue.Count; i++)
            {
                if (fValue.ElementAt(i).ElementAt(0) == '1')
                {
                    OneValue.Add(i);
                }

            }
            return OneValue;
        }

        public static List<long> FormXValue(List<long> XValue, List<string> fValue)
        {
            for (int i = 0; i < fValue.Count; i++)
            {
                if (fValue.ElementAt(i).ElementAt(0) == 'x')
                {
                    XValue.Add(i);
                }

            }
            return XValue;
        }

        public class Kmap
        {
            public Kmap() { }
            private int temp, temp1, temp2, temp3;
            public List<List<char>> invokeKmap(ref List<int> one, ref List<int> x)
            {
                char tempChar;
                List<char> result = new List<char>();
                List<List<char>> filterResult = new List<List<char>>();

                setKmap.SetOnes(one);
                setKmap.SetX(x);
                setKmap.SOP = true;

                result = compareKmapTerms.minimize(ref setKmap.ones, ref setKmap.dCare);
                Console.WriteLine("Ready");
                if (result.Count != 1)
                {
                    filterResult.Clear();
                    filterResult = FilterKmapTerms.filter(ref result, setKmap.ones);
                    string indexer = "";
                    for (int temp = 0; temp < filterResult.Count; temp++)
                    {
                        if (filterResult.Count > 1)
                        {
                            indexer += "\n" + (temp + 1).ToString() + " - ";
                            Console.Write(indexer);
                        }



                        for (int temp1 = 0; temp1 < filterResult[temp].Count; temp1++)
                        {
                            Console.Write(filterResult[temp][temp1]);

                        }

                    }
                }
                else
                {
                    Console.WriteLine(result[0]);
                }


                return filterResult;
            }
        }
    }





}
