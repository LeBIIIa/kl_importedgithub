﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System; 
using System.Collections.Generic;
using System.Linq;

namespace KL___WorkingVersion
{
    class Task1_4 : Task
    {
        private static readonly int count = 8;
        List<Symbol> symbols;
        Dictionary<int, int> ts;

        public Task1_4()
        {
            symbols = new List<Symbol>();
            ts = new Dictionary<int, int>();
        }

        class Symbol
        {
            public Symbol(int repeat, double probability, char charSymbol)
            {
                Repeat = repeat;
                Probability = probability;
                CharSymbol = charSymbol;
            }
            public override bool Equals(object obj)
            {
                if ((obj == null) || !GetType().Equals(obj.GetType()))
                {
                    return false;
                }
                else
                {
                    Symbol s = obj as Symbol;
                    return (Repeat == s.Repeat) && (Probability == s.Probability);
                }
            }

            public override int GetHashCode()
            {
                return Repeat.GetHashCode() + Probability.GetHashCode() + CharSymbol.GetHashCode();
            }

            public int Repeat { get;private set; }
            public double Probability { get;private set; }
            public char CharSymbol { get;private set; }
            public string Efficient { get; set; } = string.Empty;
        };

        static void Algorithm(List<Symbol> symbols, Dictionary<int, int> ts)
        {
            int ind = 0, i;
            Queue<List<Symbol>> nodes = new Queue<List<Symbol>>();
            nodes.Enqueue(symbols);
            while ( nodes.Count > 0 ) {
                var s = nodes.Dequeue();
                if (s.Count < 2)
                {
                    continue;
                }
                int k = s.Count / 2;
                while (true)
                {
                    double sum1 = GetSum(s.GetRange(0, k));
                    double sum2 = GetSum(s.GetRange(k, (s.Count - k)));
                    double sum3 = GetSum(s.GetRange(k - 1, (s.Count - k + 1)));
                    double sum4 = GetSum(s.GetRange(0, k - 1));
                    double eps1 = Math.Abs(sum1 - sum2);
                    double eps2 = Math.Abs(sum3 - sum4);
                    if (eps2 <= eps1 && (k - 1) > 0)
                    {
                        k--;
                    }
                    else
                    {
                        break;
                    }
                }
                int index = symbols.FindIndex(x => x.CharSymbol == s[0].CharSymbol);
                for (i = 0; i < s.Count; i++)
                {
                    if ( i < k )
                    {
                        symbols[index + i].Efficient += "1";
                    }
                    else
                    {
                        symbols[index + i].Efficient += "0";
                    }
                }
                nodes.Enqueue(s.GetRange(0, k));
                nodes.Enqueue(s.GetRange(k, s.Count - k));
                ts.Add(index+k, ++ind);
            }
        }

        static double GetSum(List<Symbol> lst)
        {
            double sum = 0;
            foreach(Symbol s in lst)
            {
                sum += Math.Round(s.Probability, 3);
            }
            return sum;
        }

        public override void Execute(Word[] word, Document pdf, Font f )
        {
            string temp;
            pdf.Add(new Paragraph("\n\n", f));
            PdfPTable wordTable = new PdfPTable(3);
            wordTable.AddCell(new Phrase("Літера", f));
            wordTable.AddCell(new Phrase("Кількість", f));
            wordTable.AddCell(new Phrase("Імовірність", f));
            int sum = 0;
            for (int i = 0; i < count; i++)
            {
                sum += word[i].Value1 * 10 + word[i].Value2;
            }
            for (int i = 0; i < count; i++)
            {
                int symbol = word[i].Value1 * 10 + word[i].Value2;
                symbols.Add(new Symbol(symbol, Math.Round((double)symbol / sum, 3) , word[i].Symbol));
            }

            symbols = symbols.OrderByDescending(o => o.Probability).ToList();
            

            for (int i = 0; i < count; i++)
            {
                wordTable.AddCell(new Phrase(word[i].Symbol.ToString(), f));
                wordTable.AddCell( new Phrase((word[i].Value1*10 + word[i].Value2).ToString(),f) );
                wordTable.AddCell(new Phrase( Convert.ToString((Math.Round((double)(word[i].Value1 * 10 + word[i].Value2)/sum , 3))), f));
            }

            wordTable.AddCell(new Phrase("Усього", f)); wordTable.AddCell(sum.ToString()); wordTable.AddCell((1.00).ToString());
            pdf.Add(wordTable);

            pdf.Add(new Paragraph("\n\n Формуємо ефективний код: \n\n", f));

            PdfPTable probabilityTable = new PdfPTable(7);
            PdfPCell cell = new PdfPCell(new Phrase("Літера", f))
            {
                Rowspan = 2
            };
            probabilityTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("Імовірність", f))
            {
                Rowspan = 2
            };
            probabilityTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("Ефективний код", f))
            {
                Colspan = 2
            };
            probabilityTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("Не ефективний код", f))
            {
                Colspan = 2
            };
            probabilityTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("Крок", f))
            {
                Rowspan = 2
            };
            probabilityTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("Код", f));
            probabilityTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("Довжина", f));
            probabilityTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("Код", f));
            probabilityTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("Довжина", f));
            probabilityTable.AddCell(cell);
            

            var map = new Dictionary<char, double>();
            for (int i = 0; i < count; i++)
            {
                map.Add(word[i].Symbol, symbols.ElementAt(i).Probability);
            }
            var list = map.Values.ToList();
            

            Algorithm(symbols, ts); 

            List<string> noEffectiveCode = new List<string>
            {
                "000",
                "001",
                "010",
                "011",
                "100",
                "101",
                "110",
                "111"
            };

            for (int i = 0; i < count; i++)
            {
                probabilityTable.AddCell(new Phrase(symbols[i].CharSymbol.ToString(), f));
                probabilityTable.AddCell(new Phrase(symbols[i].Probability.ToString(), f));
                probabilityTable.AddCell(new Phrase(symbols[i].Efficient,  f));
                probabilityTable.AddCell(new Phrase(symbols[i].Efficient.Length.ToString(), f));
                probabilityTable.AddCell(new Phrase(noEffectiveCode.ElementAt(i), f));
                probabilityTable.AddCell(new Phrase("3", f));
                Phrase phrs = new Phrase("", f);
                if (ts.ContainsKey(i))
                {
                    phrs = new Phrase(ts[i].ToString(), f);
                }
                probabilityTable.AddCell(phrs);
            }
            pdf.Add(probabilityTable);

            Image img = Image.GetInstance(Environment.CurrentDirectory + @"\Formula1.png");
            img.ScaleToFit(100f, 100f);
            img.Alignment = Element.ALIGN_LEFT;
            pdf.Add(img);

            
            temp = "-( ";
            
            for(byte i = 0; i < count; i++)
            {
                temp += symbols[i].Probability.ToString() + "*log2( " + symbols[i].Probability.ToString() + " ) + ";
            }
            temp = temp.Remove(temp.Length - 5, 5);
            temp += " ) ) = \n-(";
            double localSum = 0;
            for(byte i = 0; i < count; i++)
            {
                temp += Math.Round(symbols[i].Probability * Math.Log(symbols[i].Probability, 2), 4).ToString() + " + ";
                localSum += symbols[i].Probability * Math.Log(symbols[i].Probability, 2);
            }
            temp = temp.Remove(temp.Length - 3, 3);
            temp += " ) ) = \n - ( " + Math.Round(localSum , 4).ToString() + " ) = ";
            temp += " ( " + Math.Round(-localSum, 4).ToString() + " ) \n";
            double H = -localSum;
            pdf.Add(new Paragraph(temp,f));

            Image img1 = Image.GetInstance(Environment.CurrentDirectory + @"\Formula2.png");
            img1.ScaleToFit(200f, 200f);
            img1.Alignment = Element.ALIGN_LEFT;
            pdf.Add(img1);

            Image img2 = Image.GetInstance(Environment.CurrentDirectory + @"\Formula3.png");
            img2.ScaleToFit(100f, 100f);
            img2.Alignment = Element.ALIGN_LEFT;
            pdf.Add(img2);
            temp = "";
            localSum = 0;
            for (short i = 0; i < count; i++)
            {

                temp += symbols[i].Probability.ToString() + "*" + symbols[i].Efficient.Length.ToString() + " + ";
                localSum += symbols[i].Efficient.Length * symbols[i].Probability;
            }
            temp = temp.Remove(temp.Length - 3, 3);
            temp += " = " + localSum.ToString();
            double L_ef = localSum; 
            pdf.Add(new Paragraph(temp, f));
            if ( H > L_ef) { temp = "L сер.еф < H < L сер.не еф"; }
            else { temp = "H < L сер.еф < L сер.не еф"; }
            pdf.Add(new Paragraph(temp, f));
            temp = "Довжина повідомлення при ефективному кодуванні : ";
            int sumKoding = 0;
            for(short i = 0 ; i < 8; i++)
            {
                sumKoding += symbols[i].Efficient.Length;
            } temp += sumKoding.ToString() + " біт\nДовжина повідомлення при не ефективному кодуванні : 24 біт" ;
            pdf.Add(new Paragraph(temp, f));
        }
    }
}
