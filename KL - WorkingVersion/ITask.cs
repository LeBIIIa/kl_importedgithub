﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text;

namespace KL___WorkingVersion
{
    interface ITask
    {
        void Execute( Word[] word, Document pdf, Font f );
    }
}
