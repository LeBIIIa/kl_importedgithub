﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using iTextSharp.text;
using System.Collections.Generic;
using iTextSharp.text.pdf;

namespace Karno
{
    public static class Utils
    {

        public static string ToMintermMap(this Group group)
        {
            if (group.Count == 0)
                throw new ArgumentException("Group must contain at least one term");

            // Take the first term and "mark" all variables that change value in the group
            var terms = group.ToList();
            var first_term_list = terms.First().ToList();

            for (int n = 1; n < terms.Count; n++)
            {
                for (int i = 0; i < first_term_list.Count; i++)
                {
                    if (terms[n][i] != first_term_list[i])
                        first_term_list[i] = '-';
                }
            }

            return string.Join("", first_term_list);
        }

        public static string ToSOPExpression(this Coverage coverage)
        {
            var result = new StringBuilder();
            var groups = coverage.OrderBy(g => g.Count).ToList();
            for (int g = 0; g < groups.Count; g++)
            {
                
                result.Append(groups[g].ToMintermExpression());
                if (g != (groups.Count - 1))
                    result.Append(" + ");
            }

            return result.ToString();
        }

        public static string ToSOPExpressionInKNF(this Coverage coverage)
        {
            var result = new StringBuilder();
            var groups = coverage.OrderBy(g => g.Count).ToList();
            for (int g = 0; g < groups.Count; g++)
            {
                result.Append(groups[g].ToMintermExpressionInKNF());
                if (g != (groups.Count - 1))
                    result.Append(" * ");
            }

            return result.ToString();
        }


        public static string ToSOPExpressionOne(this Coverage coverage , int order)
        {
            var result = new StringBuilder();
            var groups = coverage.OrderBy(g => g.Count).ToList();
                result.Append(groups[order].ToMintermExpression());
            return result.ToString();
        }

        public static string ToMintermExpression(this Group group)
        {
            var map = group.ToMintermMap();

            var result = new StringBuilder(map.Length);
            for (int i = 0; i < map.Length; i++)
            {
                if (map[i] != '-')
                {
                    var letter = (char)('a' + i);
                    result.Append(map[i] == '0' ? "/" : "");
                    result.Append(letter);
                    
                }
            }

            return result.ToString();
        }

        public static string ToMintermExpressionInKNF(this Group group)
        {
            var map = group.ToMintermMap();

            string result = "(";
            for (int i = 0; i < map.Length; i++)
            {
                if (map[i] != '-')
                {
                    if (map[i] == '1')
                        result += "/";
                    result += (char)('a' + i);
                    if (i != map.Length - 1)
                        result += " v ";

                }
            }
            result += ")";

            return result.ToString();
        }

        public static void PrintCoverage(this Coverage coverage , Document pdf , Font f , List<long> OneValue , int minimaizeParameters)
        {
            List<string>[] CodeSaver = new List<string>[coverage.Count]; int counter = 0;
         //   pdf.Add(new Paragraph("DELLAY"));
            
            foreach (var g in coverage)
            {
                CodeSaver[counter] = new List<string>();
                var terms = g.ToList();
                for (int i = 0; i < terms.Count; i++)
                {
                    string code = "";
                    if (terms.ElementAt(i).ElementAt(0) == '1') code += "1";
                    else code += "";
                    string codetemp = terms.ElementAt(i).ElementAt(1).ToString() + terms.ElementAt(i).ElementAt(2).ToString() + terms.ElementAt(i).ElementAt(3).ToString() + terms.ElementAt(i).ElementAt(4).ToString();
                    switch (codetemp)
                    {
                        case "0000": { code += "0"; break; }
                        case "0001": { code += "1"; break; }
                        case "0010": { code += "2"; break; }
                        case "0011": { code += "3"; break; }
                        case "0100": { code += "4"; break; }
                        case "0101": { code += "5"; break; }
                        case "0110": { code += "6"; break; }
                        case "0111": { code += "7"; break; }
                        case "1000": { code += "8"; break; }
                        case "1001": { code += "9"; break; }
                        case "1010": { code += "A"; break; }
                        case "1011": { code += "B"; break; }
                        case "1100": { code += "C"; break; }
                        case "1101": { code += "D"; break; }
                        case "1110": { code += "E"; break; }
                        case "1111": { code += "F"; break; }
                    }

                    CodeSaver[counter].Add(code);
                }
                counter++;   
            }
            
            for(int i = 0; i < CodeSaver.Length; i++)
            {
                string nabor = "";
                string funcResult = coverage.ToSOPExpressionOne(i);
                String temp = (i+1).ToString() + ") Склеювання Клітинок: ";
                for (int j = 0; j < CodeSaver[i].Count; j++)
                {
                    if (j == CodeSaver[i].Count - 1) { temp += CodeSaver[i].ElementAt(j) + ".";
                        if (ContainValue(OneValue , CodeSaver[i].ElementAt(j)))
                        {
                            nabor += CodeSaver[i].ElementAt(j) + "  ";
                        }
                    }
                    else { temp += CodeSaver[i].ElementAt(j) + " , ";
                        if (ContainValue(OneValue, CodeSaver[i].ElementAt(j)))
                        {
                            nabor += CodeSaver[i].ElementAt(j) + "  ";
                        }
                    }
                }
                temp += "\nРезультат: " + funcResult + "\n";
                pdf.Add(new Paragraph(temp  , f));
                pdf.Add(new Paragraph("Мінімізуються набори: " + nabor, f));
                
            }

            if (minimaizeParameters == 1)
            {
                pdf.Add(new Paragraph("Результат:\nf = " + coverage.ToSOPExpression()));
            }
            else
            {
                pdf.Add(new Paragraph("Результат:\n/f = " + coverage.ToSOPExpression() , f));
                pdf.Add(new Paragraph("Результат:\nf = " + coverage.ToSOPExpressionInKNF(), f));
            }

            pdf.Add( new Paragraph("Визначення сполучних термів:", f));
            PdfPTable ConectiveTerms = new PdfPTable(2);
            ConectiveTerms.WidthPercentage = 25;
            ConectiveTerms.AddCell(new PdfPCell(new Phrase("Номер імпліканти", f)));
            ConectiveTerms.AddCell(new PdfPCell(new Phrase("abcde", f)));

            List<string> ConectiveTermsList = new List<string>();
            var groups = coverage.OrderBy(g => g.Count).ToList();
            

            for (int i = 0; i < groups.Count; i++)
            {
                ConectiveTermsList.Add((groups.ElementAt(i).ToMintermMap()));
                ConectiveTerms.AddCell(new PdfPCell(new Phrase("i" + i.ToString() , f)));
                ConectiveTerms.AddCell(new PdfPCell(new Phrase(ConectiveTermsList.ElementAt(i), f)));
            }
            pdf.Add(ConectiveTerms); pdf.Add(new Paragraph("\n", f));

            List<string> TermResult = new List<string>();  counter = 0; 
            PdfPTable TermTable = new PdfPTable(3);
            TermTable.AddCell(new PdfPCell(new Phrase("Порівнюються",f)));
            TermTable.AddCell(new PdfPCell(new Phrase("Результат", f)));
            TermTable.AddCell(new PdfPCell(new Phrase("Терм", f)));
            for(int i = 0; i < ConectiveTermsList.Count; i++)
            {
                for(int j = i+1; j < ConectiveTermsList.Count; j++)
                {
                 bool hasTerm =  CoverageTerms(ref TermResult,ConectiveTermsList.ElementAt(i) , ConectiveTermsList.ElementAt(j) ); 
                    TermTable.AddCell(new PdfPCell(new Phrase("i" + i.ToString() + " i" + j.ToString(), f)));
                    
                    TermTable.AddCell(new PdfPCell(new Phrase(TermResult.ElementAt(counter), f)));
                    if (hasTerm)
                    {
                        string tempTerm = "";
                        for(int k = 0; k < TermResult.ElementAt(counter).Length; k++)
                        {
                            if (TermResult.ElementAt(counter).ElementAt(k) == 'x')
                                tempTerm += "-";
                            else tempTerm += TermResult.ElementAt(counter).ElementAt(k);
                        }
                        TermTable.AddCell(new PdfPCell(new Phrase(tempTerm , f)));
                    }
                    else TermTable.AddCell(new PdfPCell(new Phrase("Немає", f)));
                    counter++;
                }
            }

            pdf.Add(TermTable);
        }

        public static void PrintCoverages(this KMap map, Document pdf , Font f , List<long> OneValues , int minimaizeParameter , bool only_min = true  )
        {
            var coverages = map.Minimize();
            if (coverages.Count == 0)
                return;
            var min_cost = coverages.Min(c => c.Cost.Value); int i = 0;
            foreach(var coverage in coverages)
            {
                if (i == 1) break;
                if (only_min && coverage.Cost.Value > min_cost)
                {
                    
                    continue;
                }
                coverage.PrintCoverage(pdf , f , OneValues , minimaizeParameter);
                i++;
            }
        }

        

        public static void PrintTestResults(this KMap map, bool only_min = false)
        {
            var tester = new KMapTester(map);
            (var result, var n, var coverage) = tester.Test(only_min);
            if (result)
                Console.WriteLine("TEST: OK");
            else
            {
                Console.WriteLine($"TEST: FAILED - with the following coverage:");
               // coverage.PrintCoverage(pdf);
            }
        }

        public static string ToBinaryString(this long num, int num_bits)
        {
            return Convert.ToString(num, 2).PadLeft(num_bits, '0');
        }

        public static int Hamming(string s1, string s2)
        {
            Debug.Assert(s1.Length == s2.Length);

            int d = 0;
            for (int i = 0; i < s1.Length; i++)
                if (s1[i] != s2[i])
                    d++;
            return d;
        }

        public static bool ContainValue(List<long> value , string toCompare)
        {
            List<string> newValue = new List<string>();
            for(int i = 0; i < value.Count; i++)
            {
                switch (value.ElementAt(i))
                {
                    case 0: { newValue.Add("0"); break; }
                    case 1: { newValue.Add("1"); break; }
                    case 2: { newValue.Add("2"); break; }
                    case 3: { newValue.Add("3"); break; }
                    case 4: { newValue.Add("4"); break; }
                    case 5: { newValue.Add("5"); break; }
                    case 6: { newValue.Add("6"); break; }
                    case 7: { newValue.Add("7"); break; }
                    case 8: { newValue.Add("8"); break; }
                    case 9: { newValue.Add("9"); break; }
                    case 10: { newValue.Add("A"); break; }
                    case 11: { newValue.Add("B"); break; }
                    case 12: { newValue.Add("C"); break; }
                    case 13: { newValue.Add("D"); break; }
                    case 14: { newValue.Add("E"); break; }
                    case 15: { newValue.Add("F"); break; }
                    case 16: { newValue.Add("10"); break; }
                    case 17: { newValue.Add("11"); break; }
                    case 18: { newValue.Add("12"); break; }
                    case 19: { newValue.Add("13"); break; }
                    case 20: { newValue.Add("14"); break; }
                    case 21: { newValue.Add("15"); break; }
                    case 22: { newValue.Add("16"); break; }
                    case 23: { newValue.Add("17"); break; }
                    case 24: { newValue.Add("18"); break; }
                    case 25: { newValue.Add("19"); break; }
                    case 26: { newValue.Add("1A"); break; }
                    case 27: { newValue.Add("1B"); break; }
                    case 28: { newValue.Add("1C"); break; }
                    case 29: { newValue.Add("1D"); break; }
                    case 30: { newValue.Add("1E"); break; }
                    case 31: { newValue.Add("1F"); break; }
                }
            }
            bool result = false;
            for(int i = 0; i < value.Count; i++)
            {
                if (newValue.ElementAt(i) == toCompare)
                {
                    result = true; return result;
                }
            }
            return result;


        }

        public static bool CoverageTerms(ref List<string> result , string c1 , string c2)
        {
            bool hasTerm = false;
            int counter = 0; string temp = "";
            for(int i = 0; i < c1.Length; i++)
            {
                if(c1.ElementAt(i) == '-' && c2.ElementAt(i) == '-')
                {
                    temp += "-";
                }
                else if((c1.ElementAt(i) == '-' && c2.ElementAt(i) == '0') || (c1.ElementAt(i) == '0' && c2.ElementAt(i) == '-') || (c1.ElementAt(i) == '0' && c2.ElementAt(i) == '0'))
                {
                    temp += "0";
                }
                else if ((c1.ElementAt(i) == '-' && c2.ElementAt(i) == '1') || (c1.ElementAt(i) == '1' && c2.ElementAt(i) == '-') || (c1.ElementAt(i) == '1' && c2.ElementAt(i) == '1'))
                {
                    temp += "1";
                }
                else if ((c1.ElementAt(i) == '0' && c2.ElementAt(i) == '1') || (c1.ElementAt(i) == '1' && c2.ElementAt(i) == '0') )
                {
                    temp += "x";
                    counter++;
                }
            }
            result.Add(temp);
            if (counter == 1)
            {
                hasTerm = true; 
            }
            return hasTerm;
        }

    }
}
