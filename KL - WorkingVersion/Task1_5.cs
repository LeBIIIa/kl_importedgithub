﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KL___WorkingVersion
{
    static class Task1_5
    {
        static int bits;
        static int controlBits;
        static int randomBit;
        static List<int> controlsBits;
        static List<string> hexHamming;
        private static List<int> FindControlBits(List<int> code)
        {
            List<int> control = new List<int>();
            for (int i = controlBits - 1; i >= 0; i--)
            {
                int sum = 0;
                for (int j = 1; j <= bits + controlBits; j++)
                {
                    if (hexHamming.ElementAt(j).ElementAt(i) == '1')
                    {
                        sum ^= code[j];
                    }
                }
                control.Add(sum);
            }
            return control;
        }
        private static List<int> SetControlBits(string code)
        {
            List<int> codes = new List<int>
            {
                -1
            };
            codes.AddRange(Array.ConvertAll(code.ToArray(), s => int.Parse(s.ToString())));
            for (int h = 0; h < controlBits; ++h)
            {
                codes.Insert((int)Math.Pow(2, h), 0);
            }
            controlsBits = FindControlBits(codes);
            for (int h = 0; h < controlBits; ++h)
            {
                codes[(int)Math.Pow(2, h)] = controlsBits[h];
            }
            return codes;
        }
        private static int CountControlBits()
        {
            int k = 0;
            while (true)
            {
                long powK = (long)Math.Pow(2, k);
                long N = 1 + k + bits;
                if (powK >= N)
                {
                    break;
                }
                ++k;
            }
            return k;
        }
        private static int RandomBit()
        {
            int bit = 0;
            Random rnd = new Random();
            while (true)
            {
                bit = rnd.Next(1, bits + controlBits + 1);
                if ( (bit & (bit-1) ) == 0 && bit > 0)
                {
                    continue;
                }
                break;
            }
            return bit;
        }

        private static string Printing(string temp, List<int> s, int _new, List<int> controlsBits, List<int> intBits)
        {
            for (int i = controlBits - 1; i >= 0; --i)
            {
                if (_new == 1)
                {
                    temp += "K";
                }
                else
                {
                    temp += "k";
                }
                temp += ((int)Math.Pow(2, controlBits - i - 1)).ToString() + " = ";
                for (int j = 1; j <= bits + controlBits; j++)
                {
                    if ((j > 0) && ((j & (j - 1)) == 0))
                    {
                        continue;
                    }
                    if ( hexHamming.ElementAt(j).ElementAt(i).ToString() == "1" )
                    {
                        temp += "i" + j + " # ";
                    }
                }

                temp = temp.Remove(temp.Length - 2, 2);
                temp += " = ";

                for (int j = 1; j <= bits + controlBits; j++)
                {
                    if ((j > 0) && ((j & (j - 1)) == 0))
                    {
                        continue;
                    }
                    if (hexHamming.ElementAt(j).ElementAt(i).ToString() == "1")
                    {
                        temp += intBits[j] + " # ";
                    }
                }

                temp = temp.Remove(temp.Length - 2, 2);
                temp += " = " + controlsBits[controlBits - i - 1] + "\n";
            }
            return temp;
        }

        private static string ToBinary(string k)
        {
            return Convert.ToString(Convert.ToInt32(k, 16), 2).PadLeft(4, '0');
        }


        private static void FormingTable(ref List<int> intBits, ref string temp, Word[] word, Document pdf, Font f)
        {
            string s = ToBinary(word[0].Value1.ToString()) + ToBinary(word[0].Value2.ToString()) + ToBinary(word[7].Value1.ToString()) + ToBinary(word[7].Value2.ToString());
            bits = s.Length;
            temp = word[0].Value1.ToString() + word[0].Value2.ToString() + word[7].Value1.ToString() + word[7].Value2.ToString();
            pdf.Add(new Phrase(temp, f));
            f.Size = 6;
            pdf.Add(new Phrase("16", f));
            f.Size = 12;
            temp = " = ";
            temp += s;
            pdf.Add(new Phrase(temp, f));
            f.Size = 6;
            pdf.Add(new Phrase("2\n\n", f));
            f.Size = 12;

            controlBits = CountControlBits();

            PdfPTable hammingTable = new PdfPTable(bits + controlBits)
            {
                WidthPercentage = 105
            };
            hexHamming = new List<string>(new string[] {
            "00000","00001", "00010", "00011",
            "00100", "00101", "00110", "00111",
            "01000", "01001", "01010", "01011",
            "01100", "01101", "01110", "01111" ,
            "10000", "10001", "10010", "10011" ,
            "10100", "10101"});
            PdfPCell cell = new PdfPCell();
            for (int i = 0; i < controlBits; i++)
            {
                for (int j = 1; j <= bits + controlBits; j++)
                {
                    cell = new PdfPCell(new Phrase(hexHamming.ElementAt(j).ElementAt(i).ToString(), f));
                    hammingTable.AddCell(cell);
                }
            }

            f.Size = 9;
            for (int i = 1; i <= bits + controlBits; ++i)
            {
                string str;
                if ((i > 0) && ((i & (i - 1)) == 0))
                {
                    str = "k" + i;
                }
                else
                {
                    str = "i" + i;
                }
                cell = new PdfPCell(new Phrase(str, f));
                hammingTable.AddCell(cell);
            }
            f.Size = 12;

            intBits = SetControlBits(s);
            randomBit = RandomBit();

            for (int i = 1; i < intBits.Count; ++i)
            {
                cell = new PdfPCell(new Phrase(intBits[i].ToString(), f));
                hammingTable.AddCell(cell);
            }

            for (int i = 1; i < intBits.Count; i++)
            {
                if (i != randomBit)
                {
                    cell = new PdfPCell(new Phrase("", f));
                }
                else
                {
                    if (int.Parse(intBits[randomBit].ToString()) == 1)
                    {
                        cell = new PdfPCell(new Phrase("(0)", f));
                    }
                    else
                    {
                        cell = new PdfPCell(new Phrase("(1)", f));
                    }
                }
                hammingTable.AddCell(cell);
            }

            pdf.Add(hammingTable);
            temp = string.Empty;
        }
        public static void Execute(Word[] word, Document pdf, Font f)
        {
            string temp = string.Empty;

            List<int> intBits = new List<int>();

            FormingTable(ref intBits, ref temp, word, pdf, f);

            temp = Printing(temp, intBits, 0, controlsBits, intBits);
            
            pdf.Add(new Paragraph(temp, f));

            intBits[randomBit] ^= 1;

            for (int h = 0; h < controlBits; ++h)
            {
                intBits[(int)Math.Pow(2, h)] = 0;
            }

            var newControlBits = FindControlBits(intBits);
            
            for (int h = 0; h < controlBits; ++h)
            {
                intBits[(int)Math.Pow(2, h)] = newControlBits[h];
            }

            temp = "\nПрипустимо що помилка сталася в біті i" + (randomBit) + "\nПеревірка :\n";

            temp = Printing(temp, intBits, 1, newControlBits, intBits);

            pdf.Add(new Paragraph(temp, f));

            temp = "K # k = ";

            for (int h = controlBits - 1; h >= 0; --h)
            {
                int step = (int)Math.Pow(2, h);
                temp += "(" + "K" + step + " # " + "k" + step + ")";
            }

            temp += " = ";
            
            for (int i = controlBits - 1; i >= 0; i--)
            {
                temp += "( " + newControlBits[i] + " # " + controlsBits[i] + ")";
            }
            temp += " = " + hexHamming[randomBit];
            pdf.Add(new Phrase(temp, f));
            f.Size = 8;
            pdf.Add(new Phrase("2", f));
            f.Size = 12;
            temp = "Значення " + hexHamming[randomBit] + " вказує на те що помилка сталася в " + randomBit.ToString() + " біті який є інформаційним, (" + randomBit + " = " + hexHamming[randomBit] + ")";
            pdf.Add(new Paragraph(temp, f));   
        }
    };
}
