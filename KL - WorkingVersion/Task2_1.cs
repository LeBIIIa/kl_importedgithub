﻿using iTextSharp.text;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text.pdf;

namespace KL___WorkingVersion
{
    static class Task2_1
    {
        public static void execute(Word[] word, Document pdf, Font f)
        {
            bool IsHaveZero = false; bool IsHaveOne = false; bool IsMonoton = false; bool IsSimetrical = false; bool IsLinear = false;
            String temp;
            List<int>[] number = new List<int>[8];
            for (int i = 0; i < 8; i++)
            {
                number[i] = new List<int>();
            }

            temp = "\n";


            number[0].Add(0); number[0].Add(0); number[0].Add(0);
            number[1].Add(0); number[1].Add(0); number[1].Add(1);
            number[2].Add(0); number[2].Add(1); number[2].Add(0);
            number[3].Add(0); number[3].Add(1); number[3].Add(1);
            number[4].Add(1); number[4].Add(0); number[4].Add(0);
            number[5].Add(1); number[5].Add(0); number[5].Add(1);
            number[6].Add(1); number[6].Add(1); number[6].Add(0);
            number[7].Add(1); number[7].Add(1); number[7].Add(1);

            int x1, x2; x1 = word[3].Value1; x2 = word[6].Value2;
            List<int> f1 = new List<int>(); List<int> f2 = new List<int>();
            for (int i = 0; i < 4; i++)
            {
                f1.Add(x1 % 2); f2.Add(x2 % 2);
                x1 /= 2; x2 /= 2;
            }
            f1.Reverse(); f2.Reverse();

            temp += "1ц4л - " + word[3].Value1 + " - ";
            for (int i = 0; i < 4; i++)
            {
                temp += f1.ElementAt(i);
            }
            temp += "\n";

            temp += "2ц7л - " + word[6].Value2 + " - ";
            for (int i = 0; i < 4; i++)
            {
                temp += f2.ElementAt(i);
            }
            temp += "\n\n";
            pdf.Add(new Paragraph(temp, f));

            PdfPTable wordTable = new PdfPTable(4); wordTable.WidthPercentage = 15;
            PdfPCell cell = new PdfPCell(new Phrase("a", f)); wordTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("b", f)); wordTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("c", f)); wordTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("f", f)); wordTable.AddCell(cell);
            temp = "";

            for (int i = 0; i < 8; i++)
            {

                for (int j = 0; j < 3; j++)
                {
                    temp = number[i].ElementAt(j).ToString();
                    cell = new PdfPCell(new Phrase(temp, f));
                    wordTable.AddCell(cell);
                }
                if (i < 4)
                {
                    cell = new PdfPCell(new Phrase(f1.ElementAt(i).ToString(), f));
                    wordTable.AddCell(cell);
                }
                else
                {
                    cell = new PdfPCell(new Phrase(f2.ElementAt(i % 4).ToString(), f));
                    wordTable.AddCell(cell);
                }
            }
            pdf.Add(wordTable);

            temp = "";
            int zeroRow, lastRow;
            zeroRow = f1.ElementAt(0); lastRow = f2.ElementAt(3);

            // Константа 0
            if (zeroRow == 0)
            {
                temp = "1)	Функція на нульовому наборі змінних  f(0,0,0) = 0. Отже, функція зберігає константу «0».\n";
                zeroRow = 0;
                IsHaveZero = true;
            }
            else
            {
                temp = "1)	Функція на нульовому наборі змінних  f(0,0,0) = 1. Отже, функція не зберігає константу «0».\n";
                zeroRow = 1;
            }

            // Константа 1
            if (lastRow == 1)
            {
                temp += "2)	Функція на одиничному наборі змінних  f(1,1,1) = 1. Отже, функція зберігає константу «1».\n\n";
                lastRow = 1;
                IsHaveOne = true;
            }
            else
            {
                temp += "2)	Функція на одиничному наборі змінних  f(1,1,1) = 0. Отже, функція не зберігає константу «1».\n\n";
                lastRow = 0;
            }
            pdf.Add(new Paragraph(temp, f));

            // Монотонність
            temp = "3) Монотонність функції\n\n";
            pdf.Add(new Paragraph(temp, f));
            PdfPTable pdfTable = new PdfPTable(30);

            for (int i = 0; i < 6; i++)
            {
                cell = new PdfPCell(new Phrase("a", f)); pdfTable.AddCell(cell);
                cell = new PdfPCell(new Phrase("b", f)); pdfTable.AddCell(cell);
                cell = new PdfPCell(new Phrase("c", f)); pdfTable.AddCell(cell);
                cell = new PdfPCell(new Phrase("f", f)); pdfTable.AddCell(cell);
                cell = new PdfPCell(new Phrase(" ", f)); pdfTable.AddCell(cell);

            }
            for (short i = 1; i <= 30; i++)
            {

                if (i % 5 == 0)
                {
                    cell = new PdfPCell(new Phrase(" ", f)); pdfTable.AddCell(cell);
                }
                else if (i % 4 == 0)
                {
                    f.SetStyle(2);
                    cell = new PdfPCell(new Phrase(zeroRow.ToString(), f)); pdfTable.AddCell(cell);
                }
                else
                {

                    cell = new PdfPCell(new Phrase("0", f)); pdfTable.AddCell(cell);
                }
            }
            cell = new PdfPCell(new Phrase("0", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("0", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("1", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(f1.ElementAt(1).ToString(), f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(" ", f)); pdfTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("0", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("0", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("1", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(f1.ElementAt(1).ToString(), f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(" ", f)); pdfTable.AddCell(cell);


            cell = new PdfPCell(new Phrase("0", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("1", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("0", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(f1.ElementAt(2).ToString(), f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(" ", f)); pdfTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("0", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("1", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("0", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(f1.ElementAt(2).ToString(), f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(" ", f)); pdfTable.AddCell(cell);


            cell = new PdfPCell(new Phrase("1", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("0", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("0", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(f2.ElementAt(0).ToString(), f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(" ", f)); pdfTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("1", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("0", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("0", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(f2.ElementAt(0).ToString(), f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(" ", f)); pdfTable.AddCell(cell);




            cell = new PdfPCell(new Phrase("0", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("1", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("1", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(f1.ElementAt(3).ToString(), f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(" ", f)); pdfTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("1", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("0", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("1", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(f2.ElementAt(1).ToString(), f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(" ", f)); pdfTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("0", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("1", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("1", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(f1.ElementAt(3).ToString(), f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(" ", f)); pdfTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("1", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("1", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("0", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(f2.ElementAt(2).ToString(), f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(" ", f)); pdfTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("1", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("0", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("1", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(f2.ElementAt(1).ToString(), f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(" ", f)); pdfTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("1", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("1", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("0", f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(f2.ElementAt(2).ToString(), f)); pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase(" ", f)); pdfTable.AddCell(cell);

            for (short i = 1; i <= 6; i++)
            {
                for (short j = 1; j <= 5; j++)
                {
                    if (j == 4)
                    {
                        cell = new PdfPCell(new Phrase(lastRow.ToString(), f)); pdfTable.AddCell(cell);
                    }
                    else if (j == 5)
                    {
                        cell = new PdfPCell(new Phrase(" ", f)); pdfTable.AddCell(cell);
                    }
                    else
                    {
                        cell = new PdfPCell(new Phrase("1", f)); pdfTable.AddCell(cell);
                    }
                }

            }
            cell = new PdfPCell(new Phrase("1", f)); cell.Colspan = 5; pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("2", f)); cell.Colspan = 5; pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("3", f)); cell.Colspan = 5; pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("4", f)); cell.Colspan = 5; pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("5", f)); cell.Colspan = 5; pdfTable.AddCell(cell);
            cell = new PdfPCell(new Phrase("6", f)); cell.Colspan = 5; pdfTable.AddCell(cell);
            pdf.Add(pdfTable);

            f.SetStyle(0);
            List<string> monoton = new List<string>(); string localM;
            localM = f1.ElementAt(0).ToString() + f1.ElementAt(1).ToString() + f1.ElementAt(3).ToString() + f2.ElementAt(3).ToString(); 
                
            monoton.Add(localM);

            localM = f1.ElementAt(0).ToString() + f1.ElementAt(1).ToString() + f2.ElementAt(1).ToString() + f2.ElementAt(3).ToString();
         
            monoton.Add(localM);

            localM = f1.ElementAt(0).ToString() + f1.ElementAt(2).ToString() + f1.ElementAt(3).ToString() + f2.ElementAt(3).ToString();
            
            monoton.Add(localM);

            localM = f1.ElementAt(0).ToString() + f1.ElementAt(2).ToString() + f2.ElementAt(2).ToString() + f2.ElementAt(3).ToString();
            
            monoton.Add(localM);

            localM = f1.ElementAt(0).ToString() + f2.ElementAt(0).ToString() + f2.ElementAt(1).ToString() + f2.ElementAt(3).ToString();
            
            monoton.Add(localM);

            localM = f1.ElementAt(0).ToString() + f2.ElementAt(0).ToString() + f2.ElementAt(2).ToString() + f2.ElementAt(3).ToString();
            
            monoton.Add(localM);

            int nabor = 0;
            bool isMonoton = true;
            for (short i = 0; i < 6; i++)
            {
               for(int j = 0; j < 3; j++)
                {
                    if(Convert.ToInt32(monoton[i][j]) > Convert.ToInt32(monoton[i][j+1]))
                    {
                        nabor = i + 1;
                        isMonoton = false;
                        goto checking;
                    } 
                }

            }
            checking: 
            if (isMonoton)
            {
                temp = "\n3)	Функція  є монотонною, оскільки при будь-якому зростанні кількості '1' у послідовності сусідніх наборів змінних значення функції збільшується.\n\n";
                IsMonoton = true;
            }
            else
            {
                temp = "\n3)	Функція не є монотонною, оскільки при будь-якому зростанні кількості '1' у послідовності сусідніх наборів змінних значення функції зменшується. Наприклад на наборі " + nabor.ToString() + " функція не є монотонною \n\n";
            }

            pdf.Add(new Paragraph(temp, f));

            // Самодвоїстість
            temp = "4) Самодвоїстість функції.\n";
            pdf.Add(new Paragraph(temp, f));
            PdfPTable pdfTable1 = new PdfPTable(4); pdfTable1.WidthPercentage = 30;
            cell = new PdfPCell(new Phrase("abc", f)); pdfTable1.AddCell(cell);
            cell = new PdfPCell(new Phrase("f", f)); pdfTable1.AddCell(cell);
            cell = new PdfPCell(new Phrase("abc", f)); pdfTable1.AddCell(cell);
            cell = new PdfPCell(new Phrase("f", f)); pdfTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("000", f)); pdfTable1.AddCell(cell);
            cell = new PdfPCell(new Phrase(f1.ElementAt(0).ToString(), f)); pdfTable1.AddCell(cell);
            cell = new PdfPCell(new Phrase("111", f)); pdfTable1.AddCell(cell);
            cell = new PdfPCell(new Phrase(f2.ElementAt(3).ToString(), f)); pdfTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("001", f)); pdfTable1.AddCell(cell);
            cell = new PdfPCell(new Phrase(f1.ElementAt(1).ToString(), f)); pdfTable1.AddCell(cell);
            cell = new PdfPCell(new Phrase("110", f)); pdfTable1.AddCell(cell);
            cell = new PdfPCell(new Phrase(f2.ElementAt(2).ToString(), f)); pdfTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("010", f)); pdfTable1.AddCell(cell);
            cell = new PdfPCell(new Phrase(f1.ElementAt(2).ToString(), f)); pdfTable1.AddCell(cell);
            cell = new PdfPCell(new Phrase("101", f)); pdfTable1.AddCell(cell);
            cell = new PdfPCell(new Phrase(f2.ElementAt(1).ToString(), f)); pdfTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("011", f)); pdfTable1.AddCell(cell);
            cell = new PdfPCell(new Phrase(f1.ElementAt(3).ToString(), f)); pdfTable1.AddCell(cell);
            cell = new PdfPCell(new Phrase("100", f)); pdfTable1.AddCell(cell);
            cell = new PdfPCell(new Phrase(f2.ElementAt(0).ToString(), f)); pdfTable1.AddCell(cell);
            pdf.Add(pdfTable1);

            if (f1.ElementAt(0) == f2.ElementAt(3))
            {
                temp = "\nФункція не є самодвоїстою, оскільки на першій парі протилежних наборів функція не приймає протилежні значення.\n";
            }
            else if (f1.ElementAt(1) == f2.ElementAt(2))
            {
                temp = "\nФункція не є самодвоїстою, оскільки на другій парі протилежних наборів функція не приймає протилежні значення.\n";
            }
            else if (f1.ElementAt(2) == f2.ElementAt(1))
            {
                temp = "\nФункція не є самодвоїстою, оскільки на третій парі протилежних наборів функція не приймає протилежні значення.\n";
            }
            else if (f1.ElementAt(3) == f2.ElementAt(0))
            {
                temp = "\nФункція не є самодвоїстою, оскільки на четвертій парі протилежних наборів функція не приймає протилежні значення.\n";
            }
            else
            {
                temp = "\nФункція є самодвоїстою, оскільки на всіх парах протилежних наборів приймає протилежні значення.\n";
                IsSimetrical = true;
            }
            pdf.Add(new Paragraph(temp, f));

            // Лінійність

            temp = "\n5) Лінійність функції\n";
            temp += "Для визначення лінійності функції подамо її у вигляді полінома Жегалкіна\n*Примітка (символом '/' далі позначається інверсія значення те саме що риска над символом , символом '#' позначається операція сума за модулем )\n";

            temp += "f = ";
            for (int i = 0; i < 8; i++)
            {
                if (i < 4)
                {
                    if (f1.ElementAt(i) == 1)
                    {
                        if (number[i].ElementAt(0) == 0) temp += "/a";
                        else temp += "a";

                        if (number[i].ElementAt(1) == 0) temp += "/b";
                        else temp += "b";

                        if (number[i].ElementAt(2) == 0) temp += "/c";
                        else temp += "c";

                        temp += " # ";
                    }
                }
                else
                {
                    if (f2.ElementAt(i % 4) == 1)
                    {
                        if (number[i].ElementAt(0) == 0) temp += "/a";
                        else temp += "a";

                        if (number[i].ElementAt(1) == 0) temp += "/b";
                        else temp += "b";

                        if (number[i].ElementAt(2) == 0) temp += "/c";
                        else temp += "c";

                        temp += " # ";
                    }
                }

            }
            temp += " = ";


            for (int i = 0; i < 8; i++)
            {
                if (i < 4)
                {
                    if (f1.ElementAt(i) == 1)
                    {
                        if (number[i].ElementAt(0) == 0) temp += "(a # 1)";
                        else temp += "a";

                        if (number[i].ElementAt(1) == 0) temp += "(b # 1)";
                        else temp += "b";

                        if (number[i].ElementAt(2) == 0) temp += "(c # 1)";
                        else temp += "c";

                        if (i != 7)
                            temp += " # ";
                    }
                }
                else
                {
                    if (f2.ElementAt(i % 4) == 1)
                    {
                        if (number[i].ElementAt(0) == 0) temp += "(a # 1)";
                        else temp += "a";

                        if (number[i].ElementAt(1) == 0) temp += "(b # 1)";
                        else temp += "b";

                        if (number[i].ElementAt(2) == 0) temp += "(c # 1)";
                        else temp += "c";

                        if (i != 7)
                            temp += " # ";
                    }
                }

            }
            temp += " = \n";

            int abc, ab, ac, bc, one, a, b, c; abc = 0; ab = 0; ac = 0; bc = 0; one = 0; a = 0; b = 0; c = 0;
            for (int i = 0; i < 8; i++)
            {
                if (i < 4)
                {
                    if (f1.ElementAt(i) == 1)
                    {
                        if (number[i].ElementAt(0) == 0 && number[i].ElementAt(1) == 0 && number[i].ElementAt(2) == 0)
                        {
                            abc = abc + 1; ab = ab + 1; ac = ac + 1; bc = bc + 1; a = a + 1; b = b + 1; c = c + 1; one = one + 1;
                        }
                        else if (number[i].ElementAt(0) == 0 && number[i].ElementAt(1) == 0 && number[i].ElementAt(2) == 1)
                        {
                            abc = abc + 1; ac = ac + 1; bc = bc + 1; c = c + 1;
                        }
                        else if (number[i].ElementAt(0) == 0 && number[i].ElementAt(1) == 1 && number[i].ElementAt(2) == 0)
                        {
                            abc = abc + 1; ab = ab + 1; bc = bc + 1; b = b + 1;
                        }
                        else if (number[i].ElementAt(0) == 0 && number[i].ElementAt(1) == 1 && number[i].ElementAt(2) == 1)
                        {
                            abc = abc + 1; bc = bc + 1;
                        }
                        else if (number[i].ElementAt(0) == 1 && number[i].ElementAt(1) == 0 && number[i].ElementAt(2) == 0)
                        {
                            abc = abc + 1; ab = ab + 1; ac = ac + 1; a = a + 1;
                        }
                        else if (number[i].ElementAt(0) == 1 && number[i].ElementAt(1) == 0 && number[i].ElementAt(2) == 1)
                        {
                            abc = abc + 1; ac = ac + 1;
                        }
                        else if (number[i].ElementAt(0) == 1 && number[i].ElementAt(1) == 1 && number[i].ElementAt(2) == 0)
                        {
                            abc = abc + 1; ab = ab + 1;
                        }
                        else if (number[i].ElementAt(0) == 1 && number[i].ElementAt(1) == 1 && number[i].ElementAt(2) == 1)
                        {
                            abc = abc + 1;
                        }
                    }
                }
                else
                {
                    if (f2.ElementAt(i % 4) == 1)
                    {
                        if (number[i].ElementAt(0) == 0 && number[i].ElementAt(1) == 0 && number[i].ElementAt(2) == 0)
                        {
                            abc = abc + 1; ab = ab + 1; ac = ac + 1; bc = bc + 1; a = a + 1; b = b + 1; c = c + 1; one = one + 1;
                        }
                        else if (number[i].ElementAt(0) == 0 && number[i].ElementAt(1) == 0 && number[i].ElementAt(2) == 1)
                        {
                            abc = abc + 1; ac = ac + 1; bc = bc + 1; c = c + 1;
                        }
                        else if (number[i].ElementAt(0) == 0 && number[i].ElementAt(1) == 1 && number[i].ElementAt(2) == 0)
                        {
                            abc = abc + 1; ab = ab + 1; bc = bc + 1; b = b + 1;
                        }
                        else if (number[i].ElementAt(0) == 0 && number[i].ElementAt(1) == 1 && number[i].ElementAt(2) == 1)
                        {
                            abc = abc + 1; bc = bc + 1;
                        }
                        else if (number[i].ElementAt(0) == 1 && number[i].ElementAt(1) == 0 && number[i].ElementAt(2) == 0)
                        {
                            abc = abc + 1; ab = ab + 1; ac = ac + 1; a = a + 1;
                        }
                        else if (number[i].ElementAt(0) == 1 && number[i].ElementAt(1) == 0 && number[i].ElementAt(2) == 1)
                        {
                            abc = abc + 1; ac = ac + 1;
                        }
                        else if (number[i].ElementAt(0) == 1 && number[i].ElementAt(1) == 1 && number[i].ElementAt(2) == 0)
                        {
                            abc = abc + 1; ab = ab + 1;
                        }
                        else if (number[i].ElementAt(0) == 1 && number[i].ElementAt(1) == 1 && number[i].ElementAt(2) == 1)
                        {
                            abc = abc + 1;
                        }
                    }
                }
            }

            bool isLiner = true;

            if (abc != 0)
            {
                temp += "\n";
                for (int i = 0; i < abc; i++)
                {
                    temp += "abc # ";
                }
                temp += "               ";
                if (abc % 2 == 0) { temp += "(= 0)"; }
                else { temp += "(= abc)"; isLiner = false; }
            }

            if (ab != 0)
            {
                temp += "\n";
                for (int i = 0; i < ab; i++)
                {
                    temp += "ab # ";
                }
                temp += "               ";
                if (ab % 2 == 0) { temp += "(= 0)"; }
                else { temp += "(= ab)"; isLiner = false; }
            }

            if (ac != 0)
            {
                temp += "\n";
                for (int i = 0; i < ac; i++)
                {
                    temp += "ac # ";
                }
                temp += "               ";
                if (ac % 2 == 0) { temp += "(= 0)"; }
                else { temp += "(= ac)"; isLiner = false; }
            }

            if (bc != 0)
            {
                temp += "\n";
                for (int i = 0; i < bc; i++)
                {
                    temp += "bc # ";
                }
                temp += "               ";
                if (bc % 2 == 0) { temp += "(= 0)"; }
                else { temp += "(= bc)"; isLiner = false; }
            }

            if (a != 0)
            {
                temp += "\n";
                for (int i = 0; i < a; i++)
                {
                    temp += "a # ";
                }
                temp += "               ";
                if (a % 2 == 0) { temp += "(= 0)"; }
                else { temp += "(= a)"; isLiner = false; }
            }
            if (b != 0)
            {
                temp += "\n";
                for (int i = 0; i < b; i++)
                {
                    temp += "b # ";
                }
                temp += "               ";
                if (b % 2 == 0) { temp += "(= 0)"; }
                else { temp += "(= b)"; isLiner = false; }
            }

            if (c != 0)
            {
                temp += "\n";
                for (int i = 0; i < c; i++)
                {
                    temp += "c # ";
                }
                temp += "               ";
                if (c % 2 == 0) { temp += "(= 0)"; }
                else { temp += "(= c)"; isLiner = false; }
            }
            if (one != 0)
            {
                temp += "\n";
                for (int i = 0; i < one; i++)
                {
                    temp += "1 # ";
                }
                temp += "               ";
                if (one % 2 == 0) { temp += "(= 0)"; }
                else { temp += "(= 1)"; isLiner = false; }
            }
            temp += "\n";
            if (isLiner) { temp += "Оскільки поліном не містить добутки змінних, то функція  є лінійною."; IsLinear = true; }
            else { temp += "Оскільки поліном містить добутки змінних, то функція не є лінійною."; }

            pdf.Add(new Paragraph(temp, f));

            if (!IsHaveOne && !IsHaveZero && !IsMonoton && !IsSimetrical && !IsLinear)
            {
                temp = "\nОтже, із п'яти необхідних для створення ФПС властивостей присутні всі, тому дана функція  утворює ФПС.";
            }
            else
            {
                temp = "\nОтже, із п'яти необхідних для створення ФПС властивостей відсутні такі : ";
                if (IsHaveOne) { temp += " не зберігання константи '1', "; }
                if (IsHaveZero) { temp += " не зберігання константи '0', "; }
                if (IsMonoton) { temp += " не монотонність, "; }
                if (IsSimetrical) { temp += " не самодвоїстість,  "; }
                if (IsLinear) { temp += " не лінійність, "; }
                temp += " отже дана функція не утворює ФПС";
            }
            pdf.Add(new Paragraph(temp, f));
        }
    }
}
