﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KL
{
    public class ConverteTerms
    {
        public static List<int> termToPos(List<char> terms, ref int type)
        {
            List<char> term = new List<char>();
            for (int i = 0; i < terms.Count; i++)
            {
                term.Add(terms[i]);
            }
            List<int> result = new List<int>();
            int weight = 0;
            int pos = 0;

            term = extract(ref term, ref type);

            for (int temp = term.Count - 1; temp >= 0; temp--)
            {
                if (term[temp] == '\'') //dashed digits
                {
                    pos += 0;
                    temp--; //going to the next minterm
                }
                else if (Char.IsLetter(term[temp])) //undashed digits
                {
                    pos += (int)Math.Pow(2.0, weight);
                }

                //seperators
                if (temp >= 0)
                {
                    if ((term[temp] == '+' || temp == 0) && temp >= 0)
                    {
                        result.Add(pos);
                        pos = 0; //default case
                        weight = 0; //default case
                        continue;
                    }
                }
                weight++;
            }

            for (int temp = 0; temp < result.Count - 1; temp++)
            {
                if (result[temp] == result[temp + 1])
                    result.RemoveAt(0 + temp + 1);

                else if (result[temp] > result[temp + 1])
                {
                    Swap(ref result, ref result, temp, temp + 1);
                    temp = -1;
                }
            }

            return result;
        }

        public static void Swap(ref List<int> result, ref List<int> result1, int index, int index1)
        {
            int temp = result[index];
            result[index] = result1[index1];
            result1[index1] = temp;

        }

        public static List<char> extract(ref List<char> terms, ref int type)
        {
            int temp = 0;
            List<char> part = new List<char>();
            int dashCount = 0;

            for (temp = 0; temp < terms.Count; temp++)
            {
                if (char.IsLetter(terms[temp]) || terms[temp] == '\'')
                {
                    part.Add(terms[temp]);
                    if (terms[temp] == '\'') dashCount++;
                }

                if ((terms[temp] == '+' || temp == terms.Count - 1) && part.Count - dashCount < type)
                {
                    if (terms[temp] == '+')
                        temp--; //return temp to last digit
                    compTerm(ref part, ref terms, ref type, ref temp); //complete term
                    temp--;//return temp to the digit before part
                    part.Clear(); //clear edit to be used again
                    dashCount = 0;
                }

                else if (terms[temp] == '+' && part.Count - dashCount == type)
                {
                    part.Clear();  //clear part
                    dashCount = 0;   //clear dash
                }
            }
            return terms;
        }

        public static void compTerm(ref List<char> part, ref List<char> terms, ref int type, ref int pos)
        {
            int dashCount = 0;
            List<char> copyPart = new List<char>();
            int temp;
            bool edited = false;

            pos -= part.Count - 1;

            if (pos == 19)
            {
                int r = 0;
            }
            if (pos + part.Count == terms.Count)
            {
                if (pos > 0)
                {
                    terms.RemoveRange(0 + pos, part.Count);
                }

                else
                {
                    terms.Clear();
                }
            }

            else
            {
                terms.RemoveRange(0 + pos, part.Count);
            }
            for (temp = 0; temp < part.Count; temp++)
            {
                if (part[temp] == '\'') dashCount++;

                else if (part[temp] != 65 + temp - dashCount)
                {
                    part.Insert(0 + temp, (char)(65 + temp - dashCount));
                    for (int i = 0; i < part.Count; i++)
                    {
                        copyPart.Add(part[i]);

                    }
                    //copy letter
                    part.Insert(0 + temp + 1, '\'');

                    terms.InsertRange(pos, copyPart);

                    terms.Insert(0 + pos, '+');

                    terms.InsertRange(pos, part);

                    edited = true; //this part has eddieted 
                    break;
                }
            }

            if (edited == false && part[part.Count - 1] != 65 + type)
            {
                part.Insert(0 + temp, (char)(65 + temp - dashCount)); //insert a letter
                for (int i = 0; i < part.Count; i++)
                {
                    copyPart.Add(part[i]);

                }                                 //copy letter
                part.Insert(0 + temp + 1, '\'');


                terms.InsertRange(pos, copyPart);

                terms.Insert(0 + pos, '+');


                terms.InsertRange(pos, part);


            }
        }
    }
}
