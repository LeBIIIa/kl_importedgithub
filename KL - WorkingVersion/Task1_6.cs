﻿using iTextSharp.text;
using KL___WorkingVersion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KL___WorkingVersion
{
    static class Task1_6
    {
        static SortedSet<string> st = new SortedSet<string>();
        
        static string TranslateToBin(int number)
        {
            int size = 4;
            int _base = 2;
            List<int> v = new List<int>();
            while (number > 0) {
                int ost = number % _base;
                v.Add(ost);
                number /= _base;
            }
            for (int i = v.Count; i < size; ++i) {
                v.Add(0);
            }
            v.Reverse();
            return string.Join("", v.ToArray());
        }
        static void DFS(string state, string end)
        {
            char ci;
            if (state == end)
                return;
            for (int i = 0; i < state.Length; ++i)
            {
                if (state[i] == end[i])
                    continue;
                ci = state[i];
                state = state.Remove(i, 1).Insert(i, (ci == '1') ? "0" : "1");
                if (st.Contains(state))
                {
                    state = state.Remove(i, 1).Insert(i, ci.ToString());
                    continue;
                }
                DFS(state, end);
                if (state != end)
                    st.Add(state);
                state = state.Remove(i, 1).Insert(i, ci.ToString());
            }
        }

        public static void Execute(Word[] word, Document pdf, Font f)
        {
            ShowValue(word, pdf, f);
            string temp = ""; int index = 1;

            for (int i = 0; i < 7; i++)
            {
                temp = "\n";
                
                temp += index.ToString() + ") " + word[i].Value1 + " => " + word[i].Value2 + "\n";
                Start(word[i].Value1, word[i].Value2);
                temp += TranslateToBin(word[i].Value1) + " => " + TranslateToBin(word[i].Value2) + ":\n Помилкові коди при переході : ";
                if (st.Count == 0) temp += " Помилкових кодів немає";
                for (int j = 0; j < st.Count; j++)
                    {
                        temp += "  " + st.ElementAt(j) + ",";
                    }
                    pdf.Add(new Paragraph(temp, f)); index++;
                    temp = "\n";
                
                st.Clear();
                temp += index.ToString() + ") " + word[i].Value2 + " => " + word[i+1].Value1 + "\n";
                Start(word[i].Value2, word[i+1].Value1);
                temp += TranslateToBin(word[i].Value2) + " => " + TranslateToBin(word[i+1].Value1) + ":\n Помилкові коди при переході : ";
                if (st.Count == 0) temp += " Помилкових кодів немає";
                for (int j = 0; j < st.Count; j++)
                {
                    temp += "  " + st.ElementAt(j) + "  ";
                }
                pdf.Add(new Paragraph(temp, f)); index++;
                st.Clear();
                
            }
            temp = "\n";
            temp += index.ToString() + ") " + word[7].Value1 + " => " + word[7].Value2 + "\n";
            Start(word[7].Value1, word[7].Value2);
            temp += TranslateToBin(word[7].Value1) + " => " + TranslateToBin(word[7].Value2) + ":\n Помилкові коди при переході : ";
            for (int j = 0; j < st.Count; j++)
            {
                temp += "  " + st.ElementAt(j) + "  ";
            }
            pdf.Add(new Paragraph(temp, f)); index++;
        }

        static void Start(int begin, int end) {
            string f, e;
            f = TranslateToBin(begin);
            e = TranslateToBin(end);
            DFS(f, e);
        }

        static void ShowValue(Word[] word , Document pdf , Font f)
        {
            String value = "\n";
            for (int i = 0; i < 8; i++)
            {
                value += word[i].Symbol.ToString() + " - " + word[i].Value1.ToString() + word[i].Value2.ToString() + "\n";
            }
            value += "\n";
            for (int i = 0; i < 8; i++)
            {
                value += word[i].Value1.ToString() + word[i].Value2.ToString();
            }
            pdf.Add(new Phrase(value,f));
            f.Size = 8;
            pdf.Add(new Phrase("16\n",f));
            f.Size = 12;
          //  pdf.Add(new Paragraph("*Примітка (Далі в завданні якщо після тексту Помилкові коди при переході нічого не вказано тоді це означає що при даному переході помилкових кодів немає)", f));
            f.Size = 15;
            
            value = "\nВизначення всіх можливих помилкових кодів:\n";
            Paragraph p = new Paragraph(value , f);
            p.Alignment = 1;
            pdf.Add(p);

            f.Size = 12;
        }
    }
}
