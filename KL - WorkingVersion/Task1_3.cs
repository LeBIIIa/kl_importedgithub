﻿using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KL___WorkingVersion
{
    class Task1_3 : Task
    {
        public Task1_3() { }
        public override void Execute(Word[] word, Document pdf, Font f)
        {
            string temp;
            int number = word[0].Value1 * 100000 + word[0].Value2 * 10000 + word[1].Value1 * 1000 + word[1].Value2 * 100 + word[7].Value1 * 10 + word[7].Value2;
            List<int> moduls = new List<int>
            {
                2, 3, 5, 7, 11, 13, 17
            };
            int modulsSize, n = 0, i, localSaver;
            List<int> modulResult = new List<int>();
            List<int> localVec = new List<int>();

            temp = "Шестизначне число: ";
            temp += number + "\n";
            pdf.Add(new Paragraph(temp, f));

            if (number > 510510)
            {
                moduls.Add(19);
            }
            modulsSize = moduls.Count;
            temp = "Основи\n";
            for (i = 0; i < modulsSize; i++)
            {
                temp += "p" + (i + 1) + " = " + moduls[i] + "\n";
            }
            temp += "\n" + "P = ";
            if (modulsSize == 7) {
                temp += "p1*p2*p3*p4*p5*p6*p7 = 510 510\n";
                n = 510510;
            }
            if (modulsSize == 8) {
                temp += "p1*p2*p3*p4*p5*p6*p7*p8 = 9 699 690\n";
                n = 9699690;
            }

            temp += "\nПереведення до системи залишкових класів\n*Примітка (надалі 'mod' операція взяття остачі від ділення)\n";
            for (i = 0; i < modulsSize; i++)
            {
                modulResult.Add(number % moduls[i]);
                temp += number + " mod " + moduls[i] + " = " + modulResult[i] + "\n";
            }

            temp += "\n" + number + " = ( ";
            for (i = 0; i < modulsSize; i++)
            {
                if (i == modulsSize - 1) { temp += modulResult.ElementAt(i) + ""; break; }
                temp += modulResult.ElementAt(i) + " , ";

            }
            temp += " ) " + "\n";

            temp += "\n" + "n = P = " + n + "\n";
            for (i = 0; i < modulsSize; i++)
            {
                int counter = 1;
                do
                {
                    localSaver = counter * n / moduls[i];

                    temp += "B" + (i + 1) + " = " + counter + "*" + "n/" + moduls[i] + " = " + localSaver + "         "
                        + localSaver + " mod " + moduls[i] + " = " + localSaver % moduls[i] + "\n";
                    counter++;
                } while (localSaver % moduls[i] != 1);

                localVec.Add(localSaver);
                temp += "\n";
            }
            temp += "\n\n";
            for (i = 0; i < modulsSize; i++)
            {
                temp += "B" + (i + 1) + " = " + localVec[i] + ";" + "\n";
            }
            temp += "\n";

            temp += "Зворотнє переведення\n" + "( ";
            for (i = 0; i < modulsSize; i++)
            {
                if (i == modulsSize - 1)
                {
                    temp += modulResult.ElementAt(i) + " ";
                    break;
                }
                temp += modulResult.ElementAt(i) + " , ";
            }
            temp += " ) = ("; int localResult = 0;
            for (i = 0; i < modulsSize; i++)
            {
                if (i == modulsSize - 1)
                {
                    temp += modulResult[i] + "*" + localVec[i] + " ";
                    localResult += modulResult[i] * localVec[i];
                    break;
                }
                temp += modulResult[i] + "*" + localVec[i] + " + ";
                localResult += modulResult[i] * localVec[i];
            }
            temp += " ) * mod * ( " + n + " ) = " + localResult + " mod " + n + " = " + localResult % n;
            localResult %= n;

            temp += "\n\n(";
            for (i = 0; i < modulsSize; i++)
            {
                if (i == modulsSize - 1) {
                    temp += modulResult.ElementAt(i) + " ";
                    break;
                }
                temp += modulResult.ElementAt(i) + " , ";
            }
            temp += ") = " + localResult;

            pdf.Add(new Paragraph(temp, f));
        }
    }
}
