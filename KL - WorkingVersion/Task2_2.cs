﻿using iTextSharp.text;
using KL___WorkingVersion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text.pdf;

namespace KL___WorkingVersion
{
    static class Task2_2
    {
        private static List<string> Hex32 = new List<string>(new string[] {
            "00000", "00001", "00010", "00011",
            "00100", "00101", "00110", "00111",
            "01000", "01001", "01010", "01011",
            "01100", "01101", "01110", "01111",
            "10000", "10001", "10010", "10011",
            "10100", "10101", "10110", "10111",
            "11000", "11001", "11010", "11011",
            "11100", "11101", "11110", "11111"
            });
        static List<string> HexNumber = new List<string>(new string[] {
                "0000", "0001" , "0010" , "0011" , "0100" , "0101" , "0110" , "0111" , "1000" , "1001" , "1010" , "1011" , "1100" , "1101" , "1110" , "1111"
            });

        public static void Execute(Word[] word, Document pdf, Font f, PdfWriter writer, ref List<string> funcForKarnoMap, ref List<string> ResultForMap, ref List<string> ResultForMap2_4)
        {
            int i = 0;
            string temp;
            PdfPCell cell;

            temp = "S - відлік починається безпосередньо від першого згори одиничного значення;\n- S - відлік починається від попереднього набору відносно першого згори одиничного значення;\n+S - відлік починається від наступного набору відносно першого згори одиничного значення.\n\n";
            pdf.Add(new Paragraph(temp, f));

            // Заповнення таблиці символів
            FillValueTable(pdf, f, word);

            

            pdf.Add(new Paragraph(new Phrase("\n\n", f)));
            pdf.NewPage();

            // Заповнюється таблиця функцій
            PdfPTable funcTable = new PdfPTable(11);
            cell = new PdfPCell(new Phrase("Зміщення невизначеного значення", f))
            {
                Colspan = 6
            }; funcTable.AddCell(cell);
            funcTable.AddCell(new PdfPCell(new Phrase("-S", f)));
            funcTable.AddCell(new PdfPCell(new Phrase("S", f)));
            funcTable.AddCell(new PdfPCell(new Phrase("+S", f)));
            funcTable.AddCell(new PdfPCell(new Phrase("-S", f)));
            funcTable.AddCell(new PdfPCell(new Phrase("S", f)));

            funcTable.AddCell(new PdfPCell(new Phrase("№ набору", f)));
            funcTable.AddCell(new PdfPCell(new Phrase("a", f)));
            funcTable.AddCell(new PdfPCell(new Phrase("b", f)));
            funcTable.AddCell(new PdfPCell(new Phrase("c", f)));
            funcTable.AddCell(new PdfPCell(new Phrase("d", f)));
            funcTable.AddCell(new PdfPCell(new Phrase("e", f)));

            funcTable.AddCell(new PdfPCell(new Phrase("f0", f)));
            funcTable.AddCell(new PdfPCell(new Phrase("f1", f)));
            funcTable.AddCell(new PdfPCell(new Phrase("f2", f)));
            funcTable.AddCell(new PdfPCell(new Phrase("f3", f)));
            funcTable.AddCell(new PdfPCell(new Phrase("f4", f)));

            i = 0;


            List<string>[] func = new List<string>[5]; for (; i < 5; i++) { func[i] = new List<string>(); }
            i = 0;
            int[] funcFirst = new int[5];

            for (; i < 4; i++)
            {
                func[0].Add(HexNumber.ElementAt(word[i].Value1));
                func[0].Add(HexNumber.ElementAt(word[i].Value2));

                func[1].Add(HexNumber.ElementAt(word[i].Value1));
                func[1].Add(HexNumber.ElementAt(word[i].Value2));

                func[2].Add(HexNumber.ElementAt(word[i].Value1));
                func[2].Add(HexNumber.ElementAt(word[i].Value2));
            }

            for (i = 4; i < 8; i++)
            {
                func[3].Add(HexNumber.ElementAt(word[i].Value1));
                func[3].Add(HexNumber.ElementAt(word[i].Value2));

                func[4].Add(HexNumber.ElementAt(word[i].Value1));
                func[4].Add(HexNumber.ElementAt(word[i].Value2));
            }
            i = 0;

            for (int j = 0; j < 5; j++)
            {
                while (true)
                {
                    if (func[j].ElementAt(0).ElementAt(i).ToString() == "1")
                    {
                        funcFirst[j] = i; break;
                    }
                    i++;
                }
                i = 0;
            }
            i = 0;
            funcTable.WidthPercentage = 50;
            int x = 0; int k = -1;

            List<string>[] FuncValue = new List<string>[5]; for (; i < 5; i++) { FuncValue[i] = new List<string>(); }
            i = 0;
            bool[] fPro = new bool[5];
            int[] ofset = new int[] { -1, 0, 1, -1, 0 };

            //-------------------------------------------------------
            // Заповненння таблиці значень функції
            //-------------------------------------------------------
            for (; i < 32; i++)
            {
                cell = new PdfPCell(new Phrase(i.ToString(), f)); funcTable.AddCell(cell);
                for (int j = 0; j < 5; j++)
                {
                    cell = new PdfPCell(new Phrase(Hex32.ElementAt(i).ElementAt(j).ToString(), f));
                    funcTable.AddCell(cell);
                }
                if (i % 4 == 0) k++;
                for (int l = 0; l < 5; l++)
                {
                    if (i == funcFirst[l] + ofset[l])
                    {
                        cell = new PdfPCell(new Phrase("X" + l.ToString(), f)); funcTable.AddCell(cell);
                        fPro[l] = true; funcFirst[l] = 1;
                        FuncValue[l].Add("x");
                    }
                    else
                    {
                        if (fPro[l] && funcFirst[l] % 3 == 0 && funcFirst[l] != 0) { cell = new PdfPCell(new Phrase("X" + l.ToString(), f)); funcTable.AddCell(cell); FuncValue[l].Add("x"); }
                        else
                        {
                            cell = new PdfPCell(new Phrase(func[l].ElementAt(k).ElementAt(x).ToString(), f)); funcTable.AddCell(cell);
                            FuncValue[l].Add(func[l].ElementAt(k).ElementAt(x).ToString());
                        }
                        if (fPro[l])
                        {
                            funcFirst[l]++;
                        }
                    }
                }
                funcForKarnoMap = FuncValue[0];

                x++;
                if (x == 4) x = 0;

            }
            pdf.Add(funcTable);

            List<string> funcForTask2_4 = new List<string>();
            for (i = 0; i < funcForKarnoMap.Count; i++)
            {
                if (funcForKarnoMap[i] == "1") funcForTask2_4.Add("0");
                else if (funcForKarnoMap[i] == "0") funcForTask2_4.Add("1");
                else funcForTask2_4.Add("x");
            }


            //
            // -----------------------------------------------
            //------------------------------------------------


            pdf.Add(new Paragraph("\n Будуємо таблицю, яка ілюструє знаходження простих імплікант: \n\n\n", f));
            pdf.NewPage();
            PdfContentByte con = writer.DirectContent;


            // Додавання імплікант значення яких мають 1 одиничку
            // 

            i = 0; int counter = 0;
            List<Implicant>[] imp = new List<Implicant>[5]; for (; i < 5; i++) { imp[i] = new List<Implicant>(); }
            i = 0;

            for (; i < FuncValue[0].Count; i++)
            {
                if (FuncValue[0].ElementAt(i) != "0")
                {
                    imp[0].Add(new Implicant(Hex32.ElementAt(i), "", 0));
                    imp[0].ElementAt(counter).FindNums();
                    counter++;
                }
            }
            imp[0].Sort(); i = 0;

            // Формування значення символів
            counter = 0; int indexer = 0;
            for (; i < imp[0].Count; i++)
            {
                imp[0].ElementAt(i).Symbol = Convert.ToChar(97 + counter).ToString() + indexer.ToString();
                indexer++;
                if (i != imp[0].Count - 1)
                {
                    if (imp[0].ElementAt(i).NumOfOnes != imp[0].ElementAt(i + 1).NumOfOnes)
                    {
                        counter++; indexer = 0;
                    }
                }
            }
            counter++;

            // ============================================
            // Решта таблиць , Формування попарних термів
            // ============================================
            i = 0; int checker = 0; int TableSize = 0;
            for (; i < 4; i++)
            {
                // if (!isContinue) { TableSize = i; break;  }
                checker = 0;
                for (int y = 0; y < imp[i].Count; y++)
                {
                    for (int z = 0; z < imp[i].Count; z++)
                    {
                        int HexCodeWidth = 0; string HexCodeResult;
                        HexCodeResult = imp[i].ElementAt(y).CodeWidth(imp[i].ElementAt(z), ref HexCodeWidth);
                        if (HexCodeWidth == 1 && (imp[i].ElementAt(y).Symbol.ElementAt(0) < imp[i].ElementAt(z).Symbol.ElementAt(0)))
                        {
                            imp[i + 1].Add(new Implicant(HexCodeResult, "a", 0));
                            imp[i + 1].ElementAt(checker).FindNums();
                            imp[i + 1].ElementAt(checker).Merge = imp[i].ElementAt(y).Symbol + imp[i].ElementAt(z).Symbol;
                            checker++;
                            imp[i].ElementAt(y).IsCreateImplicant = true;
                            imp[i].ElementAt(z).IsCreateImplicant = true;
                        }
                    }
                }
                indexer = 0;
                imp[i + 1].Sort();
                for (int e = 0; e < imp[i + 1].Count; e++)
                {
                    imp[i + 1].ElementAt(e).Symbol = Convert.ToChar(97 + counter).ToString() + indexer.ToString();
                    indexer++;
                    if (e != imp[i + 1].Count - 1)
                    {
                        if (imp[i + 1].ElementAt(e).NumOfOnes != imp[i + 1].ElementAt(e + 1).NumOfOnes)
                        {
                            counter++; indexer = 0;
                        }
                    }
                }
                counter++; TableSize++;
                for (int b = 0; b < imp[i].Count; b++)
                {
                    if (imp[i].ElementAt(b).IsCreateImplicant == true) { break; }
                }
            }


            TableSize = 4;
            f.Size = 8;
            PdfPTable[] Implicants = new PdfPTable[TableSize + 1]; i = 0;
            Implicants[0] = new PdfPTable(3);
            for (i = 1; i < TableSize + 1; i++) { Implicants[i] = new PdfPTable(4); }
            //=====================================
            // Виведення значень термів в таблицю 
            //====================================

            Implicants[0].TotalWidth = 90;

            Implicants[0].AddCell(new PdfPCell(new Phrase("К", f)));
            Implicants[0].AddCell(new PdfPCell(new Phrase("П", f)));
            Implicants[0].AddCell(new PdfPCell(new Phrase("У", f)));

            i = 0;
            for (; i < imp[0].Count; i++)
            {
                cell = new PdfPCell(new Phrase(imp[0].ElementAt(i).HexCode, f));
                Implicants[0].AddCell(cell);

                cell = new PdfPCell(new Phrase(imp[0].ElementAt(i).Symbol, f));
                Implicants[0].AddCell(cell);

                if (imp[0].ElementAt(i).IsCreateImplicant)
                {
                    cell = new PdfPCell(new Phrase("+", f));
                }
                else
                {
                    cell = new PdfPCell(new Phrase("-", f));
                }
                Implicants[0].AddCell(cell);
            }

            Implicants[0].WriteSelectedRows(0, -1, 25, 820, con);

            i = 1; int step = 0;
            for (; i < TableSize; i++)
            {
                if (imp[i].Count == 0) break;
                Implicants[i].TotalWidth = 110;

                Implicants[i].AddCell(new PdfPCell(new Phrase("С", f)));
                Implicants[i].AddCell(new PdfPCell(new Phrase("К", f)));
                Implicants[i].AddCell(new PdfPCell(new Phrase("П", f)));
                Implicants[i].AddCell(new PdfPCell(new Phrase("У", f)));

                for (int j = 0; j < imp[i].Count; j++)
                {
                    cell = new PdfPCell(new Phrase(imp[i].ElementAt(j).Merge, f));
                    Implicants[i].AddCell(cell);

                    cell = new PdfPCell(new Phrase(imp[i].ElementAt(j).HexCode, f));
                    Implicants[i].AddCell(cell);

                    cell = new PdfPCell(new Phrase(imp[i].ElementAt(j).Symbol, f));
                    Implicants[i].AddCell(cell);


                    if (imp[i].ElementAt(j).IsCreateImplicant)
                    {
                        cell = new PdfPCell(new Phrase("+", f));
                    }
                    else
                    {
                        cell = new PdfPCell(new Phrase("-", f));
                    }
                    Implicants[i].AddCell(cell);

                }
                Implicants[i].WriteSelectedRows(0, -1, 115 + step, 820, con);
                step += 110;
            }

            pdf.NewPage();
            //===============================================================
            //===============================================================
            // ФОРМУВАННЯ ДИЗЮНКТИВНОЇ НОРМАЛЬНОЇ ФОРМИ
            //===============================================================
            //===============================================================

            // ПОШУК УНІКАЛЬНИХ ІМПЛІКАНТ
            List<MinTerm> minTerms = new List<MinTerm>();

            List<Implicant> DnfImplicant = new List<Implicant>(); i = 0; counter = 0;
            string[] SymbolDnf;

            for (; i < imp.Length; i++)
            {
                for (int j = 0; j < imp[i].Count; j++)
                {
                    if (!DnfImplicant.Seek(imp[i].ElementAt(j)) && !imp[i].ElementAt(j).IsCreateImplicant)
                    {
                        DnfImplicant.Add(imp[i].ElementAt(j));
                    }
                }
            }

            SymbolDnf = new string[DnfImplicant.Count]; i = 0;
            for (; i < DnfImplicant.Count; i++)
            {
                string dnf = DnfImplicant.ElementAt(i).HexCode;
                SymbolDnf[i] = "";
                for (int j = 0; j < 5; j++)
                {

                    if (dnf.ElementAt(j) == '_')
                    {
                        SymbolDnf[i] += "_";
                    }
                    else
                    {
                        if (dnf.ElementAt(j) == '1')
                        {
                            switch (j)
                            {
                                case 0: { SymbolDnf[i] += "a"; break; }
                                case 1: { SymbolDnf[i] += "b"; break; }
                                case 2: { SymbolDnf[i] += "c"; break; }
                                case 3: { SymbolDnf[i] += "d"; break; }
                                case 4: { SymbolDnf[i] += "e"; break; }
                            }
                        }
                        else
                        {
                            switch (j)
                            {
                                case 0: { SymbolDnf[i] += "/a"; break; }
                                case 1: { SymbolDnf[i] += "/b"; break; }
                                case 2: { SymbolDnf[i] += "/c"; break; }
                                case 3: { SymbolDnf[i] += "/d"; break; }
                                case 4: { SymbolDnf[i] += "/e"; break; }
                            }
                        }
                    }
                }
            }

            PdfPTable DnfTable = new PdfPTable(DnfImplicant.Count + 1)
            {
                WidthPercentage = 105
            }; f.Size = 8;
            cell = new PdfPCell(new Phrase("Одиничні набори", f))
            {
                Rowspan = 2
            }; DnfTable.AddCell(cell);

            i = 0;
            for (; i < DnfImplicant.Count; i++)
            {
                cell = new PdfPCell(new Phrase("I" + (i + 1).ToString(), f));
                DnfTable.AddCell(cell);
            }

            ////////////
            // для 2.3
            ///////////
            ConcatenceTerms[] Terms = new ConcatenceTerms[SymbolDnf.Length];

            for (i = 0; i < DnfImplicant.Count; i++)
            {
                Terms[i] = new ConcatenceTerms
                {
                    Term = SymbolDnf[i]
                };
                cell = new PdfPCell(new Phrase(SymbolDnf[i], f));
                DnfTable.AddCell(cell);

            }
            DnfTable.AddCell(new PdfPCell(new Phrase("abcde", f)));
            for (i = 0; i < DnfImplicant.Count; i++)
            {
                Terms[i].BinaryTerm = DnfImplicant.ElementAt(i).HexCode;
                cell = new PdfPCell(new Phrase(DnfImplicant.ElementAt(i).HexCode, f));
                DnfTable.AddCell(cell);
            }

            List<Implicant> selection = new List<Implicant>();
            for (i = 0; i < FuncValue[0].Count; i++)
            {
                temp = FuncValue[0].ElementAt(i).ElementAt(0).ToString();
                if (FuncValue[0].ElementAt(i).ElementAt(0) == '1')
                {
                    selection.Add(new Implicant(Hex32.ElementAt(i), "", 0));
                }
            }
            List<string> terms = new List<string>();
            temp = ""; counter = 0;
            for (i = 0; i < selection.Count; i++)
            {
                string term = "";
                temp += "(";
                cell = new PdfPCell(new Phrase(selection.ElementAt(i).HexCode, f));
                DnfTable.AddCell(cell);
                for (int j = 0; j < DnfImplicant.Count; j++)
                {
                    if (selection.ElementAt(i).IsCoverImplicant(DnfImplicant.ElementAt(j)))
                    {
                        Terms[j].Coverage.Add(selection.ElementAt(i).HexCode);
                        Terms[j].NumberOfCoverage++;
                        cell = new PdfPCell(new Phrase(" * ", f));
                        minTerms.Add(new MinTerm(DnfImplicant.ElementAt(j).HexCode, selection.ElementAt(i).HexCode, "i" + (j + 1).ToString(), i));
                        temp += minTerms.ElementAt(counter).Value + " v ";
                        term += minTerms.ElementAt(counter).Value + ";";
                        counter++;

                    }
                    else
                    {
                        cell = new PdfPCell(new Phrase("  ", f));
                    }
                    DnfTable.AddCell(cell);
                }
                if (!term.Equals(""))
                {
                    terms.Add(term);
                }
                temp = temp.Remove(temp.Length - 3, 3);
                temp += ")";
            }
            DnfTable.AddCell(new PdfPCell(new Phrase("Літер в імплікації", f)));
            for (i = 0; i < DnfImplicant.Count; i++)
            {
                cell = new PdfPCell(new Phrase(DnfImplicant.ElementAt(i).NumOfValue().ToString(), f));
                DnfTable.AddCell(cell);
            }

            pdf.Add(DnfTable);
            f.Size = 12;
            pdf.Add(new Paragraph("\n\nОтримуємо функцію F", f));

            /////////////////////////////////////////////

            SortingTerm(ref terms);
            
            for (i = 0; i < terms.Count; ++i)
            {
                if (terms[i].Length == 3)
                {
                    string comp = terms[i].Substring(0, terms[i].Length - 1);
                    
                    for (int j = i+1; j < terms.Count; ++j)
                    {
                        int count = (terms[j].Split(new string[] { comp }, StringSplitOptions.None)).Count() - 1;
                        if ( count != 0 )
                        {
                            terms.RemoveAt(j);
                        }
                    }
                }
            }


            var words1 = new List<string>();
            var words2 = new List<string>();
            
            StringBuilder sb = new StringBuilder();

            //for (i = 0;i<terms.Count;++i) {

            //    words1 = terms[i].Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            //    sb.Clear();
            //    if ( words1.Count == 1 )
            //    {
            //        continue;
            //    }
            //    string r1 = words1[words1.Count - 1].Remove(0, 1);

            //    int z = DnfImplicant[int.Parse(r1) - 1].NumOfValue();

            //    for (int g = 0; g < words1.Count; ++g)
            //    {
            //        int ind = int.Parse(words1[g].Remove(0, 1));
            //        if (DnfImplicant[ind - 1].NumOfValue() <= z)
            //        {
            //            sb.Append(words1[g] + ";");
            //        }
            //    }
            //    terms.RemoveAt(i);
            //    terms.Insert(i, sb.ToString());
            //}


            //SortingTerm(ref terms);
            //sb.Clear();
            
            for (i = 1;i<terms.Count;++i) {
                words1 = terms[i - 1].Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                words2 = terms[i].Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                for (int j = 0; j < words1.Count; j++)
                {
                    for (int g = 0; g < words2.Count; ++g)
                    {
                        sb.Append(CreateString(words1[j], words2[g]) + ";");
                    }
                }
                terms.RemoveAt(i);
                terms.RemoveAt(i - 1);
                terms.Add(sb.ToString());
                sb.Clear();
                i--;
            }

            temp += "\n\nПісля спрощення F = ";

            int minF = 0;
            words1 = terms[0].Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            int minm = Math.Min(100, words1.Count);
            for (int j = 0; j < minm; ++j)
            {
                words2 = words1[j].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();

                for (int g = 0; g < words2.Count; ++g)
                {
                    sb.Append(words2[g]);
                }
                sb.Append(" v ");
            }

            temp += sb;

            temp = temp.Remove(temp.Length - 3, 3);
            if ( words1.Count > 100)
            {
                temp += "...";
            }

            temp += "\n";

            HashSet<string> minimum = new HashSet<string>();
            
            words1 = terms[0].Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            var min = words1.ToList();
            min.Sort(delegate (string s1, string s2) {
                return s1.Length.CompareTo(s2.Length);
            });
            int size = min[0].Length;
            for (int j = 0; j < min.Count; ++j)
            {
                if (min[j].Length == size)
                    minimum.Add(min[j]);
            }
            


            minF = minimum.Count;

            temp += "Таким чином, дана функція f має " + minF;
            if (minF == 1) { temp += " мінімальну"; }
            else if (minF < 5) { temp += " мінімальні"; }
            else { temp += " мінімальних"; }
            temp += " ДНФ:\n";

            for (i = 0; i < minimum.Count; ++i)
            {
                words2 = minimum.ElementAt(i).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                for (int g = 0; g < words2.Count; ++g)
                {
                    temp += words2[g] + " v ";
                }
                temp = temp.Remove(temp.Length - 3, 3);
                temp += " = ";
                for (int g = 0; g < words2.Count; ++g)
                {
                    string s1 = minTerms.Find(o => o.Value == words2[g]).Implicant;
                    for (int t = 0; t < s1.Length; ++t)
                    {
                        if (s1.ElementAt(t) == '_') { continue; }
                        if (s1.ElementAt(t) == '1')
                        {
                            switch (t)
                            {
                                case 0: { temp += "a"; break; }
                                case 1: { temp += "b"; break; }
                                case 2: { temp += "c"; break; }
                                case 3: { temp += "d"; break; }
                                case 4: { temp += "e"; break; }
                            }
                        }
                        else
                        {
                            switch (t)
                            {
                                case 0: { temp += "/a"; break; }
                                case 1: { temp += "/b"; break; }
                                case 2: { temp += "/c"; break; }
                                case 3: { temp += "/d"; break; }
                                case 4: { temp += "/e"; break; }
                            }
                        }
                    }
                    temp += " v ";
                }
                temp = temp.Remove(temp.Length - 3, 3);
                temp += "\n";
            }
            pdf.Add(new Paragraph(temp, f));

            ResultForMap = ModifyConcatenceTerm(Terms);
            ResultForTask2_4(funcForTask2_4, ref ResultForMap2_4);
        }

        public static void ResultForTask2_4(List<string> funcForTask2_4, ref List<string> ResultForMap)
        {
            int i = 0; int counter = 0;
            List<Implicant>[] imp = new List<Implicant>[5]; for (; i < 5; i++) { imp[i] = new List<Implicant>(); }
            i = 0;

            for (; i < funcForTask2_4.Count; i++)
            {
                if (funcForTask2_4.ElementAt(i) != "0")
                {
                    imp[0].Add(new Implicant(Hex32.ElementAt(i), "", 0));
                    imp[0].ElementAt(counter).FindNums();
                    counter++;
                }
            }
            imp[0].Sort(); i = 0;

            // Формування значення символів
            counter = 0; int indexer = 0;
            for (; i < imp[0].Count; i++)
            {
                imp[0].ElementAt(i).Symbol = Convert.ToChar(97 + counter).ToString() + indexer.ToString();
                indexer++;
                if (i != imp[0].Count - 1)
                {
                    if (imp[0].ElementAt(i).NumOfOnes != imp[0].ElementAt(i + 1).NumOfOnes)
                    {
                        counter++; indexer = 0;
                    }
                }
            }
            counter++;

            // ============================================
            // Решта таблиць , Формування попарних термів
            // ============================================
            i = 0; int checker = 0; int TableSize = 0;
            for (; i < 4; i++)
            {
                // if (!isContinue) { TableSize = i; break;  }
                checker = 0;
                for (int y = 0; y < imp[i].Count; y++)
                {
                    for (int z = 0; z < imp[i].Count; z++)
                    {
                        int HexCodeWidth = 0; string HexCodeResult;
                        HexCodeResult = imp[i].ElementAt(y).CodeWidth(imp[i].ElementAt(z), ref HexCodeWidth);
                        if (HexCodeWidth == 1 && (imp[i].ElementAt(y).Symbol.ElementAt(0) < imp[i].ElementAt(z).Symbol.ElementAt(0)))
                        {
                            imp[i + 1].Add(new Implicant(HexCodeResult, "a", 0));
                            imp[i + 1].ElementAt(checker).FindNums();
                            imp[i + 1].ElementAt(checker).Merge = imp[i].ElementAt(y).Symbol + imp[i].ElementAt(z).Symbol;
                            checker++;
                            imp[i].ElementAt(y).IsCreateImplicant = true;
                            imp[i].ElementAt(z).IsCreateImplicant = true;
                        }
                    }
                }
                indexer = 0;
                imp[i + 1].Sort();
                for (int e = 0; e < imp[i + 1].Count; e++)
                {
                    imp[i + 1].ElementAt(e).Symbol = Convert.ToChar(97 + counter).ToString() + indexer.ToString();
                    indexer++;
                    if (e != imp[i + 1].Count - 1)
                    {
                        if (imp[i + 1].ElementAt(e).NumOfOnes != imp[i + 1].ElementAt(e + 1).NumOfOnes)
                        {
                            counter++; indexer = 0;
                        }
                    }
                }
                counter++; TableSize++;
                for (int b = 0; b < imp[i].Count; b++)
                {
                    if (imp[i].ElementAt(b).IsCreateImplicant == true) { break; }
                }
            }

            List<MinTerm> minTerms = new List<MinTerm>();

            List<Implicant> DnfImplicant = new List<Implicant>(); i = 0; counter = 0;
            string[] SymbolDnf;

            for (; i < imp.Length; i++)
            {
                for (int j = 0; j < imp[i].Count; j++)
                {
                    if (!DnfImplicant.Seek(imp[i].ElementAt(j)) && !imp[i].ElementAt(j).IsCreateImplicant)
                    {
                        DnfImplicant.Add(imp[i].ElementAt(j));
                    }
                }
            }

            SymbolDnf = new string[DnfImplicant.Count]; i = 0;
            for (; i < DnfImplicant.Count; i++)
            {
                string dnf = DnfImplicant.ElementAt(i).HexCode;
                SymbolDnf[i] = "";
                for (int j = 0; j < 5; j++)
                {

                    if (dnf.ElementAt(j) == '_')
                    {
                        SymbolDnf[i] += "_";
                    }
                    else
                    {
                        if (dnf.ElementAt(j) == '1')
                        {
                            switch (j)
                            {
                                case 0: { SymbolDnf[i] += "a"; break; }
                                case 1: { SymbolDnf[i] += "b"; break; }
                                case 2: { SymbolDnf[i] += "c"; break; }
                                case 3: { SymbolDnf[i] += "d"; break; }
                                case 4: { SymbolDnf[i] += "e"; break; }
                            }
                        }
                        else
                        {
                            switch (j)
                            {
                                case 0: { SymbolDnf[i] += "/a"; break; }
                                case 1: { SymbolDnf[i] += "/b"; break; }
                                case 2: { SymbolDnf[i] += "/c"; break; }
                                case 3: { SymbolDnf[i] += "/d"; break; }
                                case 4: { SymbolDnf[i] += "/e"; break; }
                            }
                        }
                    }
                }
            }


            //
            //
            //

            ConcatenceTerms[] Terms = new ConcatenceTerms[SymbolDnf.Length];

            for (i = 0; i < DnfImplicant.Count; i++)
            {
                Terms[i] = new ConcatenceTerms(){ Term = SymbolDnf[i] };


            }

            for (i = 0; i < DnfImplicant.Count; i++)
            {
                Terms[i].BinaryTerm = DnfImplicant.ElementAt(i).HexCode;

            }

            List<Implicant> selection = new List<Implicant>();
            string temp;
            for (i = 0; i < funcForTask2_4.Count; i++)
            {
                temp = funcForTask2_4.ElementAt(i).ElementAt(0).ToString();
                if (funcForTask2_4.ElementAt(i).ElementAt(0) == '1')
                {
                    selection.Add(new Implicant(Hex32.ElementAt(i), "", 0));
                }
            }
            List<string> terms = new List<string>();
            temp = ""; counter = 0;
            for (i = 0; i < selection.Count; i++)
            {
                string term = "";
                temp += "(";

                for (int j = 0; j < DnfImplicant.Count; j++)
                {
                    if (selection.ElementAt(i).IsCoverImplicant(DnfImplicant.ElementAt(j)))
                    {
                        Terms[j].Coverage.Add(selection.ElementAt(i).HexCode);
                        Terms[j].NumberOfCoverage++;

                        minTerms.Add(new MinTerm(DnfImplicant.ElementAt(j).HexCode, selection.ElementAt(i).HexCode, "i" + (j + 1).ToString(), i));
                        temp += minTerms.ElementAt(counter).Value + " v ";
                        term += minTerms.ElementAt(counter).Value + ";";
                        counter++;

                    }
                    else
                    {

                    }

                }
                if (!term.Equals(""))
                {
                    terms.Add(term);
                }
                temp = temp.Remove(temp.Length - 3, 3);
                temp += ")";
            }

            for (i = 0; i < DnfImplicant.Count; i++)
            {

            }

            ResultForMap = ModifyConcatenceTerm(Terms);

        }

        public static string CreateString(string s1, string s2)
        {
            List<string> words1 = s1.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            List<string> words2 = s2.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            for (int i = 0; i < words1.Count; ++i)
            {
                int index = words2.IndexOf(words1[i]);
                if (index != -1)
                {
                    words2.RemoveAt(index);
                }
            }
            words1 = words1.Concat(words2).ToList();
            var specialOrder = "0123456789";
            words1 = words1.OrderBy(s => s.Length).ThenBy(s => specialOrder.IndexOf(s[0])).ThenBy(s => s).ToList();
            StringBuilder res = new StringBuilder();
            foreach (string str in words1)
            {
                res.Append(str + ",");
            }
            res = res.Remove(res.Length - 1, 1);
            return res.ToString();
        }

        public static void FillValueTable(Document pdf, Font f, Word[] word)
        {
            int i = 0;
            PdfPCell cell;
            PdfPTable symbolTable = new PdfPTable(16);
            for (; i < 8; i++)
            {
                cell = new PdfPCell(new Phrase(word[i].Symbol.ToString(), f))
                {
                    Colspan = 2
                }; symbolTable.AddCell(cell);
            }
            i = 0;
            for (; i < 8; i++)
            {
                cell = new PdfPCell(new Phrase(word[i].Value1.ToString(), f));
                symbolTable.AddCell(cell);
                cell = new PdfPCell(new Phrase(word[i].Value2.ToString(), f));
                symbolTable.AddCell(cell);
            }

            List<string> HexNumber = new List<string>(new string[] {
                "0000", "0001" , "0010" , "0011" , "0100" , "0101" , "0110" , "0111" , "1000" , "1001" , "1010" , "1011" , "1100" , "1101" , "1110" , "1111"
            });
            i = 0;
            for (; i < 4; i++)
            {
                for (byte j = 0; j < 8; j++)
                {
                    cell = new PdfPCell(new Phrase(HexNumber.ElementAt(word[j].Value1).ElementAt(i).ToString(), f));
                    symbolTable.AddCell(cell);
                    cell = new PdfPCell(new Phrase(HexNumber.ElementAt(word[j].Value2).ElementAt(i).ToString(), f));
                    symbolTable.AddCell(cell);
                }
            }
            pdf.Add(symbolTable);

        }

        public static List<string> ModifyConcatenceTerm(ConcatenceTerms[] con)
        {
            List<string> Result = new List<string>();
            bool isContinue = true;
            while (isContinue)
            {
                int MaxCount = 0;
                int index = 0;
                for (int i = 0; i < con.Length; i++)
                {
                    if (con[i].NumberOfCoverage >= MaxCount)
                    {
                        MaxCount = con[i].NumberOfCoverage;
                        index = i;
                    }
                }
                Result.Add(con[index].Term);
                for (int i = 0; i < con[index].Coverage.Count; i++)
                {
                    string temp = con[index].Coverage[i];
                    for (int j = 0; j < con.Length; j++)
                    {
                        for (int k = 0; k < con[j].Coverage.Count; k++)
                        {
                            if (temp == con[j].Coverage[k])
                            {
                                con[j].Coverage.RemoveAt(k);
                                con[j].NumberOfCoverage--;
                            }
                        }
                    }
                }
                isContinue = false;
                for (int i = 0; i < con.Length; i++)
                {
                    if (con[i].Coverage.Count != 0)
                    {
                        isContinue = true;
                        break;
                    }
                }
            }
            return Result;
        }
        private static void SortingTerm(ref List<string> terms)
        {
            terms.Sort(delegate (string s1, string s2)
            {
                int i1 = 0;
                int i2 = 0;
                foreach (char c in s1)
                    if (c == 'i') i1++;
                foreach (char c in s2)
                    if (c == 'i') i2++;
                int comp = i1.CompareTo(i2);
                if (comp == 0)
                {
                    return s1.CompareTo(s2);
                }
                return comp;
            });
            terms = terms.Distinct().ToList();
        }
    }

    public class Implicant : IComparable
    {
        public string Symbol{ get; set; }
        public string HexCode { get; set; }
        public int NumOfOnes { get; set; }
        public bool IsCreateImplicant { get; set; }
        public string Merge { get; set; }
        public Implicant() { }
        public Implicant(string code, string symbol, int num)
        {
            HexCode = code;
            Symbol = symbol;
            NumOfOnes = num;
            IsCreateImplicant = false;
            Merge = "";
        }
        public void FindNums()
        {
            for (int i = 0; i < 5; i++)
            {
                if (HexCode.ElementAt(i) == '1')
                {
                    NumOfOnes++;
                }
            }
        }
        public static bool operator >(Implicant i1, Implicant i2)
        {
            return i1.NumOfOnes > i2.NumOfOnes;
        }
        public static bool operator <(Implicant i1, Implicant i2)
        {
            return i1.NumOfOnes < i2.NumOfOnes;
        }
        public int CompareTo(object obj)
        {
            Implicant forCompare = obj as Implicant;
            if (forCompare.NumOfOnes < NumOfOnes)
            {
                return 1;
            }
            if (forCompare.NumOfOnes > NumOfOnes)
            {
                return -1;
            }

            return 0;
        }
        public string CodeWidth(Implicant i, ref int codeWidth)
        {
            string resultHexCode = "";
            for (int k = 0; k < 5; k++)
            {
                if (this.HexCode.ElementAt(k) != i.HexCode.ElementAt(k))
                {
                    resultHexCode += "_";
                    codeWidth++;
                }
                else resultHexCode += this.HexCode.ElementAt(k);
            }
            return resultHexCode;
        }
        public bool IsCoverImplicant(Implicant imp)
        {
            bool result = true;
            for (int i = 0; i < this.HexCode.Length; i++)
            {
                if (this.HexCode.ElementAt(i) != imp.HexCode.ElementAt(i) && imp.HexCode.ElementAt(i) != '_') { result = false; break; }
            }
            return result;
        }
        public int NumOfValue()
        {
            NumOfOnes = 0;
            for (int i = 0; i < HexCode.Length; i++)
            {
                if (HexCode.ElementAt(i) == '1' || HexCode.ElementAt(i) == '0')
                {
                    NumOfOnes++;
                }

            }
            return NumOfOnes;
        }

    }

    public class MinTerm
    {
        public string Implicant { get; set; }
        public string Term { get; set; }
        public string Value { get; set; }
        public int SerialNumber { get; set; }
        MinTerm() { }
        public MinTerm(string implicant, string term, string value, int serial)
        {
            this.Implicant = implicant;
            this.Term = term;
            this.Value = value;
            this.SerialNumber = serial;
        }
    }

    public static class SeekImplicant
    {
        public static bool Seek(this List<Implicant> imp, Implicant obj)
        {
            for (int i = 0; i < imp.Count; i++)
            {
                if (imp.ElementAt(i).HexCode == obj.HexCode) return true;
            }
            return false;

        }
    }

    public class ConcatenceTerms
    {
        public string Term { get; set; }
        public string BinaryTerm { get; set; }
        public List<string> Coverage = new List<string>();
        public int NumberOfCoverage = 0;
        public ConcatenceTerms(String Binary, String term, List<string> coverage, int num)
        {
            Term = term;
            Coverage = coverage;
            NumberOfCoverage = num;
            BinaryTerm = Binary;
        }
        public ConcatenceTerms() { }
    }


}
