﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KL
{
    public static class ListExtra
    {
        public static void Resize<T>(this List<T> list, int sz, T c)
        {
            int cur = list.Count;
            if (sz < cur)
                list.RemoveRange(sz, cur - sz);
            else if (sz > cur)
            {
                if (sz > list.Capacity)//this bit is purely an optimisation, to avoid multiple automatic capacity changes.
                    list.Capacity = sz;
                list.AddRange(Enumerable.Repeat(c, sz - cur));
            }
        }
        public static void Resize<T>(this List<T> list, int sz) where T : new()
        {
            Resize(list, sz, new T());
        }
    }

    public class compareKmapTerms : setKmap
    {
        private static int temp, temp1, temp2, temp3;
        public static int resultTerms;


        public static List<char> minimize(ref List<int> ones, ref List<int> dCare)
        {
            List<List<int>> terms = new List<List<int>>();
            List<char> result;

            termCount = 0;
            setTerms(ones, ref terms);
            setTerms(dCare, ref terms);
            Compare(ref terms);

            unRepeat(ref terms);

            result = posToTerm(ref terms);

            return result;

        }

        private static void unRepeat(ref List<List<int>> terms)
        {

            for (int temp = 0; temp < terms.Count; temp++)
                for (int temp1 = temp + 1; temp1 < terms.Count; temp1++)
                {
                    string s1 = "";
                    string s2 = "";
                    for (int i = 0; i < terms[temp].Count; i++)
                    {
                        s1 += terms[temp].ElementAt(i).ToString();
                        s2 += terms[temp1].ElementAt(i).ToString();
                    }
                    if (s1 == s2)        //checking for repeating terms
                    {
                        terms.RemoveAt(0 + temp); //remove repeating terms
                        temp1--;
                    }
                }
        }

        public static void Compare(ref List<List<int>> terms)
        {
            List<int> tempSave = new List<int>();
            List<List<int>> saver = new List<List<int>>();
            List<bool> Hascompare = new List<bool>();

            for (temp3 = 0; temp3 < type && terms.Count > 1; temp3++)
            {
                saverCount = 0;
                Hascompare.Clear();
                Hascompare.Resize(terms.Count, false);



                //do essential minimizations
                for (temp = 0; temp < terms.Count - 1; temp++)
                {//for 1
                    for (temp1 = temp + 1; temp1 < terms.Count; temp1++)
                    {//for 2

                        tempSave.Clear();
                        for (temp2 = 0; temp2 < type; temp2++)
                        {//for 3

                            //searching for identical coordinate and save its arrangement in tempSave
                            // temp and temp1 cover terms
                            //temp2 cover coordinates
                            if ((terms[temp][temp2] == terms[temp1][temp2])) //search for mathcing element
                            {
                                //save identical
                                tempSave.Add(temp2);
                            }

                        }//end for 3
                        if (tempSave.Count == type - 1)
                            saveValue(ref tempSave, ref saver, ref Hascompare, ref terms);

                    }//end for 2
                }//end for 1

                //add remain terms that hasn't comparelified with other
                addOther(ref saver, ref Hascompare, ref saverCount, ref terms);

                //clear terms before comparelified
                terms.Clear();
                //assuming terms after comparelified
                for (int i = 0; i < saver.Count; i++)
                {
                    terms.Add(saver[i]);
                }

                saver.Clear();
            }


        }

        public static void saveValue(ref List<int> tempSave, ref List<List<int>> saver, ref List<bool> hasCompare, ref List<List<int>> terms)
        {
            if (tempSave.Count == type - 1)
            {

                saver.Add(new List<int>());

                /*set don't care arrangements after getting identicals arrangements*/

                for (temp2 = 0; temp2 < terms[temp].Count; temp2++)
                {
                    //don't care locates after matching coordinates
                    if (temp2 == tempSave.Count)
                        tempSave.Add(-1);

                    //don't care locates between matching coordinates
                    else if (temp2 != tempSave[temp2])
                        tempSave.Insert(0 + temp2, -1);
                }//end adding don't care

                /*converte identical coordinates from its arrangement to its values*/
                for (temp2 = 0; temp2 < tempSave.Count; temp2++)
                {//saver for
                    if (tempSave[temp2] == -1)
                        saver[saverCount].Add(-1);
                    else
                        saver[saverCount].Add(terms[temp][temp2]);
                }//end saver for

                hasCompare[temp] = hasCompare[temp1] = true;   //assuming them as comparelified
                saverCount++; //increase saving comparelified terms

            }
        }

        public static void addOther(ref List<List<int>> saver, ref List<bool> hasCompare, ref int saverCount, ref List<List<int>> terms)
        {
            for (temp2 = 0; temp2 < terms.Count; temp2++)
            {
                if (hasCompare[temp2] == false)
                {
                    saver.Add(new List<int>());

                    for (int temp4 = 0; temp4 < terms[temp2].Count; temp4++)
                    {
                        saver[saverCount].Add(terms[temp2][temp4]);
                    }
                    saverCount++;
                }
            }
        }

        public static List<char> posToTerm(ref List<List<int>> term)
        {
            List<char> result = new List<char>(); //for returinging
            int smallLetter = 0; //representing with small letters
            int digit,          //for pushing back
                dashCount = 0;  //dash cound
            bool isFull = true, //for checking full maps
                isEmpty = false;        //checking empty maps

            resultTerms = 0;
            //terms loop
            for (temp = 0; temp < term.Count; temp++)
            {
                //minterms loop
                for (temp1 = 0; temp1 < term[temp].Count; temp1++)
                {//minterms loop

                    //more than 9 k-map type
                    if (smallLetter == 0 && temp - dashCount > 9)
                        smallLetter = 1;
                    smallLetter = 1;//start represent with small letter


                    digit = temp1 + 65;

                    if (term[temp][temp1] == 0)
                    {
                        result.Add((char)digit); //add minterm
                        result.Add((char)39);    //add dash
                        dashCount++;            //increase dash count
                    }

                    //undashed minterms
                    if (term[temp][temp1] == 1)
                    {
                        result.Add((char)digit); //add minterm
                    }
                }//end minterms loop

                //add + for seperating between terms
                if (temp < term.Count - 1)
                    result.Add('+');

                resultTerms++;
            }//end terms loop

            //no function case
            if (ones.Count == 0)
            {
                result.Add('0');
                isEmpty = true;
            }
            //checking full map 
            for (int temp = 0; temp < term.Count && isEmpty == false; temp++)
            {
                isFull = true;   //default case
                for (int temp1 = 0; temp1 < term[temp].Count; temp1++)
                {
                    if (term[temp][temp1] != -1)
                        isFull = false;
                }
            }

            //empty maps
            if (isEmpty)
            {
                result.Clear();
                result.Add('0');
            }

            //full-map case
            else if (isFull)
            {
                result.Clear();
                result.Add('1');
            }

            return result;
        }
    }
}
