﻿namespace KL___WorkingVersion
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NameLabel = new System.Windows.Forms.Label();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.variant1 = new System.Windows.Forms.RadioButton();
            this.variant2 = new System.Windows.Forms.RadioButton();
            this.variant3 = new System.Windows.Forms.RadioButton();
            this.variant4 = new System.Windows.Forms.RadioButton();
            this.variant5 = new System.Windows.Forms.RadioButton();
            this.variant6 = new System.Windows.Forms.RadioButton();
            this.variant7 = new System.Windows.Forms.RadioButton();
            this.variant8 = new System.Windows.Forms.RadioButton();
            this.StartBtn = new System.Windows.Forms.Button();
            this.ProgressBar = new System.Windows.Forms.ProgressBar();
            this.VariantBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.TaskBox = new System.Windows.Forms.GroupBox();
            this.TaskComboBox = new System.Windows.Forms.ComboBox();
            this.CheckListTask = new System.Windows.Forms.CheckedListBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.VariantBox.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.TaskBox.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // NameLabel
            // 
            this.NameLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.NameLabel.AutoSize = true;
            this.NameLabel.BackColor = System.Drawing.SystemColors.Control;
            this.NameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NameLabel.ForeColor = System.Drawing.SystemColors.Desktop;
            this.NameLabel.Location = new System.Drawing.Point(73, 11);
            this.NameLabel.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(545, 32);
            this.NameLabel.TabIndex = 0;
            this.NameLabel.Text = "Поле вводу Прізвища імя та по-батькові";
            // 
            // NameTextBox
            // 
            this.NameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NameTextBox.Location = new System.Drawing.Point(7, 63);
            this.NameTextBox.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(677, 39);
            this.NameTextBox.TabIndex = 1;
            this.NameTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NameTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox1_KeyPress);
            // 
            // variant1
            // 
            this.variant1.AutoSize = true;
            this.variant1.BackColor = System.Drawing.SystemColors.Control;
            this.variant1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.variant1.Location = new System.Drawing.Point(7, 8);
            this.variant1.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.variant1.Name = "variant1";
            this.variant1.Size = new System.Drawing.Size(56, 33);
            this.variant1.TabIndex = 2;
            this.variant1.Text = "1";
            this.variant1.UseVisualStyleBackColor = false;
            this.variant1.CheckedChanged += new System.EventHandler(this.Variant_CheckedChanged);
            // 
            // variant2
            // 
            this.variant2.AutoSize = true;
            this.variant2.Location = new System.Drawing.Point(7, 57);
            this.variant2.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.variant2.Name = "variant2";
            this.variant2.Size = new System.Drawing.Size(56, 33);
            this.variant2.TabIndex = 3;
            this.variant2.Text = "2";
            this.variant2.UseVisualStyleBackColor = true;
            this.variant2.CheckedChanged += new System.EventHandler(this.Variant_CheckedChanged);
            // 
            // variant3
            // 
            this.variant3.AutoSize = true;
            this.variant3.Location = new System.Drawing.Point(7, 106);
            this.variant3.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.variant3.Name = "variant3";
            this.variant3.Size = new System.Drawing.Size(56, 33);
            this.variant3.TabIndex = 4;
            this.variant3.Text = "3";
            this.variant3.UseVisualStyleBackColor = true;
            this.variant3.CheckedChanged += new System.EventHandler(this.Variant_CheckedChanged);
            // 
            // variant4
            // 
            this.variant4.AutoSize = true;
            this.variant4.Location = new System.Drawing.Point(7, 155);
            this.variant4.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.variant4.Name = "variant4";
            this.variant4.Size = new System.Drawing.Size(56, 33);
            this.variant4.TabIndex = 5;
            this.variant4.Text = "4";
            this.variant4.UseVisualStyleBackColor = true;
            this.variant4.CheckedChanged += new System.EventHandler(this.Variant_CheckedChanged);
            // 
            // variant5
            // 
            this.variant5.AutoSize = true;
            this.variant5.Location = new System.Drawing.Point(7, 204);
            this.variant5.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.variant5.Name = "variant5";
            this.variant5.Size = new System.Drawing.Size(56, 33);
            this.variant5.TabIndex = 6;
            this.variant5.Text = "5";
            this.variant5.UseVisualStyleBackColor = true;
            this.variant5.CheckedChanged += new System.EventHandler(this.Variant_CheckedChanged);
            // 
            // variant6
            // 
            this.variant6.AutoSize = true;
            this.variant6.Location = new System.Drawing.Point(7, 253);
            this.variant6.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.variant6.Name = "variant6";
            this.variant6.Size = new System.Drawing.Size(56, 33);
            this.variant6.TabIndex = 7;
            this.variant6.Text = "6";
            this.variant6.UseVisualStyleBackColor = true;
            this.variant6.CheckedChanged += new System.EventHandler(this.Variant_CheckedChanged);
            // 
            // variant7
            // 
            this.variant7.AutoSize = true;
            this.variant7.Location = new System.Drawing.Point(7, 302);
            this.variant7.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.variant7.Name = "variant7";
            this.variant7.Size = new System.Drawing.Size(56, 33);
            this.variant7.TabIndex = 8;
            this.variant7.Text = "7";
            this.variant7.UseVisualStyleBackColor = true;
            this.variant7.CheckedChanged += new System.EventHandler(this.Variant_CheckedChanged);
            // 
            // variant8
            // 
            this.variant8.AutoSize = true;
            this.variant8.Location = new System.Drawing.Point(7, 351);
            this.variant8.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.variant8.Name = "variant8";
            this.variant8.Size = new System.Drawing.Size(56, 36);
            this.variant8.TabIndex = 9;
            this.variant8.Text = "8";
            this.variant8.UseVisualStyleBackColor = true;
            this.variant8.CheckedChanged += new System.EventHandler(this.Variant_CheckedChanged);
            // 
            // StartBtn
            // 
            this.StartBtn.BackColor = System.Drawing.Color.Crimson;
            this.StartBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.StartBtn.ForeColor = System.Drawing.SystemColors.InfoText;
            this.StartBtn.Location = new System.Drawing.Point(8, 8);
            this.StartBtn.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.StartBtn.Name = "StartBtn";
            this.StartBtn.Size = new System.Drawing.Size(534, 91);
            this.StartBtn.TabIndex = 11;
            this.StartBtn.Text = "Сформувати PDF";
            this.StartBtn.UseVisualStyleBackColor = false;
            this.StartBtn.Click += new System.EventHandler(this.StartBtn_Click);
            // 
            // ProgressBar
            // 
            this.ProgressBar.Location = new System.Drawing.Point(12, 115);
            this.ProgressBar.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(530, 65);
            this.ProgressBar.TabIndex = 12;
            // 
            // VariantBox
            // 
            this.VariantBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.VariantBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.VariantBox.Controls.Add(this.tableLayoutPanel4);
            this.VariantBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.VariantBox.Location = new System.Drawing.Point(14, 15);
            this.VariantBox.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.VariantBox.Name = "VariantBox";
            this.VariantBox.Padding = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.VariantBox.Size = new System.Drawing.Size(232, 454);
            this.VariantBox.TabIndex = 14;
            this.VariantBox.TabStop = false;
            this.VariantBox.Text = "Варіант";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.variant8, 0, 7);
            this.tableLayoutPanel4.Controls.Add(this.variant7, 0, 6);
            this.tableLayoutPanel4.Controls.Add(this.variant2, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.variant3, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.variant6, 0, 5);
            this.tableLayoutPanel4.Controls.Add(this.variant5, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.variant4, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.variant1, 0, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(8, 39);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 8;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(191, 398);
            this.tableLayoutPanel4.TabIndex = 13;
            // 
            // TaskBox
            // 
            this.TaskBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.TaskBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.TaskBox.Controls.Add(this.TaskComboBox);
            this.TaskBox.Controls.Add(this.CheckListTask);
            this.TaskBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TaskBox.Location = new System.Drawing.Point(273, 7);
            this.TaskBox.Margin = new System.Windows.Forms.Padding(7);
            this.TaskBox.Name = "TaskBox";
            this.TaskBox.Padding = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TaskBox.Size = new System.Drawing.Size(405, 467);
            this.TaskBox.TabIndex = 15;
            this.TaskBox.TabStop = false;
            this.TaskBox.Text = "Завдання";
            // 
            // TaskComboBox
            // 
            this.TaskComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TaskComboBox.FormattingEnabled = true;
            this.TaskComboBox.Location = new System.Drawing.Point(10, 43);
            this.TaskComboBox.Name = "TaskComboBox";
            this.TaskComboBox.Size = new System.Drawing.Size(378, 40);
            this.TaskComboBox.TabIndex = 12;
            this.TaskComboBox.SelectedValueChanged += new System.EventHandler(this.TaskComboBox_SelectedValueChanged);
            // 
            // CheckListTask
            // 
            this.CheckListTask.CheckOnClick = true;
            this.CheckListTask.FormattingEnabled = true;
            this.CheckListTask.Items.AddRange(new object[] {
            "1.1",
            "1.2",
            "1.3",
            "1.4",
            "1.5",
            "1.6",
            "2.1",
            "2.2",
            "2.3",
            "2.4"});
            this.CheckListTask.Location = new System.Drawing.Point(10, 130);
            this.CheckListTask.MultiColumn = true;
            this.CheckListTask.Name = "CheckListTask";
            this.CheckListTask.Size = new System.Drawing.Size(378, 310);
            this.CheckListTask.TabIndex = 11;
            this.CheckListTask.SelectedIndexChanged += new System.EventHandler(this.CheckListTask_SelectedIndexChanged);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(697, 800);
            this.tableLayoutPanel1.TabIndex = 16;
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.AutoSize = true;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this.StartBtn);
            this.panel1.Controls.Add(this.ProgressBar);
            this.panel1.Location = new System.Drawing.Point(74, 609);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(549, 188);
            this.panel1.TabIndex = 17;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.NameLabel, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.NameTextBox, 0, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(691, 110);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tableLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.62663F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 62.37337F));
            this.tableLayoutPanel3.Controls.Add(this.VariantBox, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.TaskBox, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 119);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 484F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(691, 484);
            this.tableLayoutPanel3.TabIndex = 17;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(144F, 144F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(723, 825);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Комп логіка";
            this.VariantBox.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.TaskBox.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.RadioButton variant1;
        private System.Windows.Forms.RadioButton variant2;
        private System.Windows.Forms.RadioButton variant3;
        private System.Windows.Forms.RadioButton variant4;
        private System.Windows.Forms.RadioButton variant5;
        private System.Windows.Forms.RadioButton variant6;
        private System.Windows.Forms.RadioButton variant7;
        private System.Windows.Forms.RadioButton variant8;
        private System.Windows.Forms.Button StartBtn;
        private System.Windows.Forms.ProgressBar ProgressBar;
        private System.Windows.Forms.GroupBox VariantBox;
        private System.Windows.Forms.GroupBox TaskBox;
        private System.Windows.Forms.CheckedListBox CheckListTask;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox TaskComboBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
    }
}

