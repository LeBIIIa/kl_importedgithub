﻿using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KL___WorkingVersion
{

    static class Extender
    {
        public static IEnumerable<string> SplitInParts( this string s, int partLength )
        {
            for (var i = 0; i < s.Length; i += partLength)
                yield return s.Substring(i, Math.Min(partLength, s.Length - i));
        }
    }
    class Task1_1 : Task
    {
        public Task1_1() { }

        public static string TranslateTo(List<int> Binary, int parts, int fromBase, int toBase )
        {
            string temp = string.Empty;
            var res = string.Join("", Binary.ToArray());
            
            var split = res.SplitInParts(parts);

            
            foreach (string st in split)
            {
                temp += Convert.ToString(Convert.ToInt32(st, fromBase), toBase).ToUpper();
                
            }
            
            return temp;
        }
        public override void Execute(Word[] word, Document pdf, Font f)
        {
            string temp;
            int after, i, add, index = 0;
            double before, result;
            List<int> Binary = new List<int>();
            List<int> BinaryDrib = new List<int>();

            result = word[0].Value1 * 100 + word[0].Value2 * 10 + word[1].Value1 + (double)word[1].Value2 / 10 + (double)word[7].Value1 / 100 + (double)word[7].Value2 / 1000;

            temp = "\nЗ кодової таблиці маємо:\n" + word[0].Symbol + " - " + word[0].Value1 + word[0].Value2 + "\t  "
            + word[1].Symbol + " - " + word[1].Value1 + word[1].Value2 + "\t    "
            + word[7].Symbol + " - " + word[7].Value1 + word[7].Value2 + "\t\n"
            + "Число: " + result + "\n" + "Переведення в двійкову систему: \nПереведення цілої частини: *Примітка (Надалі в квадратних дужках вказується остача від ділення)\n";

            pdf.Add(new Paragraph(temp, f));

            after = (int)result;

            while (after > 0)
            {
                Binary.Add(after % 2);
                temp = after + "/2 = " + after / 2 + " [" + Binary.ElementAt(index) + "] \n";
                pdf.Add(new Paragraph(temp, f));
                after /= 2;
                ++index;
            }
            after = (int)result;
            Binary.Reverse();
            temp = after + " (В десятковій системі) = ";
            for (i = 0; i < Binary.Count; i++)
            {
                temp += Binary.ElementAt(i);
            }
            temp += " (В двійковій системі)";

            pdf.Add(new Paragraph(temp, f));

            temp = "Переведення дробової частини: *Примітка (Надалі в квадратних дужках вказується ціла частина числа після множення)\n";
            before = Math.Round(result - (int)result, 3);
            index = 0;
            for (i = 0; i < 5; i++)
            {
                temp += before + " * 2= " + before * 2;
                before = Math.Round(before * 2, 3);
                if (before.CompareTo(1.0f) > 0)
                {
                    BinaryDrib.Add(1);
                    before -= 1;
                }
                else { BinaryDrib.Add(0); }
                temp += " [" + BinaryDrib.ElementAt(index) + "]\n";
                index++;
            }
            for ( i = 0; i < 7; i++) BinaryDrib.Add(0);
            pdf.Add(new Paragraph(temp, f));
            before = Math.Round(result - (int)result, 3);
            temp = before + " = 0.";
            for (i = 0; i < 5; i++)
            {
                temp += BinaryDrib.ElementAt(i);
            }
            pdf.Add(new Paragraph(temp, f));
            temp = "Число " + result + " в двійковій сиситемі числення з точністю 5 знаків після коми ";
            for (i = 0; i < Binary.Count; i++)
            {
                temp += Binary.ElementAt(i);
            }
            temp += ",";
            for (i = 0; i < 5; i++)
            {
                temp += BinaryDrib.ElementAt(i);
            }
            pdf.Add(new Paragraph(temp, f));

            /////////////////////////////////////////////
            temp = "\nПереведення в вісімкову систему:\n";
            temp += "*Примітка Переведення: розбиття на тріади , починаючи від коми (ліворуч і праворуч ) причому у дробовій частинні останню тріаду доповнюють нулями при необхідності\n";
            pdf.Add(new Paragraph(temp, f));

            add = 3 - Binary.Count % 3;
            if ( add == 3 ) { add = 0; }

            for (i = 0;i<add;++i)
            {
                Binary.Insert(0, 0);
            }

            add = 3 - BinaryDrib.Count % 3;
            if (add == 3) { add = 0; }

            for (i = 0; i < add; ++i)
            {
                BinaryDrib.Add(0);
            }
            
            temp = "";
            for (i = 0; i < Binary.Count; ++i)
            { // Виведення тріад цілої частини
                if (i % 3 == 0)
                {
                    temp += "  ";
                }
                temp += Binary.ElementAt(i);
            }
            temp += " , ";
            for (i = 0; i < 9; ++i)
            { // Виведення тріад дробової частини
                if (i % 3 == 0)
                {
                    temp += "  ";
                }
                temp += BinaryDrib.ElementAt(i);
            }
            temp += " = ";

            temp += TranslateTo(Binary, 3, 2, 8  );  // Переведення в вісімкову систему цілої частини

            temp += " , ";
            
            temp += TranslateTo(BinaryDrib, 3, 2, 8 ); //Переведення в вісімкову систему дробової частини

            //////////////////////////////////////////////
            pdf.Add(new Paragraph(temp, f));
            temp = "\nПереведення в шістнадцяткову систему числення\n";
            
            add = 4 - Binary.Count % 4;
            if (add == 4) { add = 0; }

            for (i = 0; i < add; ++i)
            {
                Binary.Insert(0, 0);
            }

            add = 4 - BinaryDrib.Count % 4;
            if (add == 4) { add = 0; }

            for (i = 0; i < add; ++i)
            {
                BinaryDrib.Add(0);
            }
            //Виведення тетрад цілої частини
            for (i = 0; i < Binary.Count; ++i)
            {
                if (i % 4 == 0)
                {
                    temp += "  ";
                }
                temp += Binary.ElementAt(i);
            }
            temp += " , ";
            //Виведення тетрад дробової частини
            for (i = 0; i < BinaryDrib.Count; i++)
            {
                if (i % 4 == 0)
                {
                    temp += "  ";
                }
                temp += BinaryDrib.ElementAt(i);
            }
            temp += " = ";
            ////

            temp += TranslateTo(Binary, 4, 2, 16 );
            
            temp += " , ";
            
            temp += TranslateTo(BinaryDrib, 4, 2, 16 ); //Переведення в 16 систему дробової частини

            temp += "\n*Примітка Переведення: аналогічне попередньому методу, тільки розбиваєм на тетради.";
            pdf.Add(new Paragraph(temp, f));
        }
    }
}
