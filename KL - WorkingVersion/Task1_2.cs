﻿using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KL___WorkingVersion
{
    class Task1_2 : Task
    {
        public override void Execute(Word[] word, Document pdf, Font f)
        {
            int i;
            string temp;
            double locali, DecimalResult;
            double result = word[0].Value1 * 100 + word[0].Value2 * 10 + word[1].Value1 + (double)word[1].Value2 / 10 + (double)word[7].Value1 / 100 + (double)word[7].Value2 / 1000;
            List<int> enumu = new List<int>
            {
                word[0].Value1,
                word[0].Value2,
                word[1].Value1,
                word[1].Value2,
                word[7].Value1,
                word[7].Value2
            };
            List<int> Binary = new List<int>();


            temp = "\nЧисло: " + result;

            temp += "\nПереведення до двійкової системи по тетрадах:\n";
            //Двійкова система ціла частина
            
            for (i = 0; i < enumu.Count; i++)
            {
                switch (enumu.ElementAt(i))
                {
                    case 0: { Binary.Add(0); Binary.Add(0); Binary.Add(0); Binary.Add(0); break; }
                    case 1: { Binary.Add(0); Binary.Add(0); Binary.Add(0); Binary.Add(1); break; }
                    case 2: { Binary.Add(0); Binary.Add(0); Binary.Add(1); Binary.Add(0); break; }
                    case 3: { Binary.Add(0); Binary.Add(0); Binary.Add(1); Binary.Add(1); break; }
                    case 4: { Binary.Add(0); Binary.Add(1); Binary.Add(0); Binary.Add(0); break; }
                    case 5: { Binary.Add(0); Binary.Add(1); Binary.Add(0); Binary.Add(1); break; }
                    case 6: { Binary.Add(0); Binary.Add(1); Binary.Add(1); Binary.Add(0); break; }
                    case 7: { Binary.Add(0); Binary.Add(1); Binary.Add(1); Binary.Add(1); break; }
                    case 8: { Binary.Add(1); Binary.Add(0); Binary.Add(0); Binary.Add(0); break; }
                    case 9: { Binary.Add(1); Binary.Add(0); Binary.Add(0); Binary.Add(1); break; }
                }
            }

            temp += result + "(в шістнадцятковій системі) = ";
            for (i = 0; i < 17; i++)
            {
                if (i == 12) temp += " , ";
                else if (i % 4 == 0) temp += "  ";
                temp += Binary.ElementAt(i);

            }
            temp += " (в двійковій системі з точністю 5 розрядів після коми)\n";

            for (i = 17; i < 24; i++)
            {
                Binary.RemoveAt(i);
                Binary.Insert(i, 0);
            }

            temp += "\nПереведення з двійкової в вісімкову з точністю 3 розряди після коми:\n";


            for (i = 0; i < 21; i++)
            {
                if (i == 12) temp += " , ";
                else if (i % 3 == 0) temp += "  ";
                temp += Binary.ElementAt(i);

            }
            temp += "(в двійковоій системі) = ";

            for (i = 0; i < 21; i += 3)
            {
                int local = Binary.ElementAt(i) * 100 + Binary.ElementAt(i + 1) * 10 + Binary.ElementAt(i + 2);
                if (i == 12) temp += ",";
                switch (local)
                {

                    case 0: { temp += 0; break; }
                    case 1: { temp += 1; break; }
                    case 10: { temp += 2; break; }
                    case 11: { temp += 3; break; }
                    case 100: { temp += 4; break; }
                    case 101: { temp += 5; break; }
                    case 110: { temp += 6; break; }
                    case 111: { temp += 7; break; }
                }
            }

            temp += " (в вісімковій системі)\n\n";
            temp += "Переведення з шістнадцяткової до десяткової з точністю 3 знаки після коми\n";
            locali = 256 * word[0].Value1 + 16 * word[0].Value2 + word[1].Value1 + (double)1 / 16 * word[1].Value2 + (double)1 / 256 * word[7].Value1 + (double)1 / 4096 * word[7].Value2;
            DecimalResult = Math.Round(locali, 3);
            
            temp += "256*" + word[0].Value1 + " + " + "16*" + word[0].Value2 + " + " + word[1].Value1 + " , " + word[1].Value2 + "/16" +
                " + " + word[7].Value1 + "/256" + " + " + word[7].Value2 + "/4096 = " + DecimalResult.ToString()  + " (в десятковій системі числення)";

            pdf.Add(new Paragraph(temp, f));

        }
    }
}
