﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text;

namespace KL___WorkingVersion
{
    abstract class Task : ITask
    {
        public abstract void Execute( Word[] word, Document pdf, Font f );
    }
}
